/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _PROTO_H
#define _PROTO_H

#define PROTO_MAGIC		0xff
#define PROTO_IPV6		0x60

#define PROTO_CMD_LOGIN		0x1
#define PROTO_CMD_HEARTBEAT	0x2
#define PROTO_CMD_DISCONNECT	0x3

#define PROTO_CMD_LOGIN_OK	0x1
#define PROTO_CMD_LOGIN_FAIL	0x2
#define PROTO_CMD_LOGIN_MULTI	0x3

extern int proto_login (char *data, char *name, char *pwd);
extern int proto_cmd_disconnect2 ();
extern int proto_parse (char *data, unsigned len);

#endif
