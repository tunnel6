/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "db.h"
#include "ipv6.h"
#include "proto.h"
#include "base64.h"
#include "defaults.h"

extern client_t client_list;
static struct stat buf;
static time_t db_mtime;

/* load database of clients from file */
static int db_file_load ()
{
	char *filename = DEFAULT_DATABASE_PATH;
	unsigned l, line = 0;
	char *buffer, *c;
	char ipv6[64];
	char prefix[80];
	char name[256];
	char pwd[256];
	ipv6_addr_t cl;
	ipv6_prefix_t pr;
	
	/* open specified file */
	FILE *fp = fopen (filename, "r");
	
	if (!fp)
		return -1;

	buffer = (char *) calloc (600, sizeof (char));

	if (!buffer)
		return -1;
	
	while (!feof (fp)) {
		/* read line */
		char *r = fgets (buffer, 600, fp);
		
		if (!r)
			continue;
		
		line ++;
		
		l = strlen (buffer);
		
		if (l < 8)
			continue;

		if (buffer[l - 1] == '\n')
			buffer[l -- - 1] = '\0';
		
		/* move to first valid letter */
		for (c = buffer; *c && *c == ' '; c ++);

		/* allow comments */
		if (*c == '#')
			continue;
		
		/* parse line */
		sscanf (c, "%s %s %s %s", name, pwd, ipv6, prefix);

		/* convert ipv6 ascii address to binary format */
		if (inet_pton (AF_INET6, ipv6, cl) != 1) {
			printf ("WARNING -> DB syntax error, line: %d\n", line);
			goto clear;
		}

		/* get routed prefix */
		if (strcmp (prefix, "-")) {
			char *s = strchr (prefix, '/');
			
			if (!s) {
				printf ("WARNING -> DB syntax error, line: %d\n", line);
				goto clear;
			}
			
			*s = '\0';
			
			/* convert ipv6 prefix ascii address to binary format */
			if (inet_pton (AF_INET6, prefix, pr) != 1) {
				printf ("WARNING -> DB syntax error, line: %d\n", line);
				goto clear;
			}
			
			s ++;
			
			/* get and set prefix length into last byte */
			pr[15] = (unsigned char) atoi (s);
		} else
			memset (pr, 0x0, sizeof (ipv6_prefix_t));
		
		client_t *c = db_find_byname (name);

		if (c) {
		  	db_update (c, cl, pr, pwd);
			goto clear;
		}
		
		db_add (cl, pr, name, pwd);		/* add new db entry */

	clear:
		/* clear line */
		memset (buffer, 0x0, l);
	}
      
	free (buffer);
	fclose (fp);

	return 0;
}

/* save database of clients to file */
static int db_file_save ()
{
	char *filename = DEFAULT_DATABASE_PATH;
	char *buffer;
	char ipv6[80];
	char prefix[80];
	ipv6_prefix_t pr;
	
	/* open specified file */
	FILE *fp = fopen (filename, "w+");
	
	if (!fp)
		return -1;

	buffer = (char *) calloc (1024, sizeof (char));

	if (!buffer)
		return -1;
	
	client_t *client;
	for (client = client_list.next; client != &client_list; client = client->next) {
		inet_ntop (AF_INET6, client->ip, ipv6, 80);

		memcpy (pr, client->prefix, sizeof (ipv6_prefix_t));
		
		if (pr[15]) {
			unsigned char l = pr[15];
			pr[15] = 0;
		  
			inet_ntop (AF_INET6, pr, prefix, 80);

			sprintf (buffer, "%s %s %s %s/%d\n", client->name, client->pwd, ipv6, prefix, l);
		} else
			sprintf (buffer, "%s %s %s -\n", client->name, client->pwd, ipv6);
		
		fputs (buffer, fp);
	}
	
	free (buffer);
	fclose (fp);

	return 0;
}

/* reload database on demand */
static int db_file_reload ()
{
	if (lstat (DEFAULT_DATABASE_PATH, &buf) == -1) {
		printf ("DB backend -> problem with db file %s\n", DEFAULT_DATABASE_PATH);
		return -1;
	}
	
	/* compare modify times - when its different, update db */
	if (buf.st_mtime == db_mtime)
		return 0;
	
	/* (re)load database file */
	if (db_file_load () == -1) {
		printf ("DB backend -> database file '%s' is missing !\n\tSyntax per line: name password clientipv6 -/routedprefix\n", DEFAULT_DATABASE_PATH);
		return -1;
	}
	
	db_mtime = buf.st_mtime;
	
	return 0;
}

static int db_file_init ()
{
	/* null modify time for first time */
	db_mtime = 0;

	return 0;
}

db_backend_t db_file = {
	.load = db_file_load,
	.save = db_file_save,
	.reload = db_file_reload,
	.init = db_file_init,
};