/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include "db.h"
#include "ipv6.h"
#include "admin.h"
#include "base64.h"
#include "defaults.h"

int admin_db_add (int argc, char **argv)
{
	if (argc != 5 && argc != 6) {
		printf ("> ERROR -> Please enter required parameters, syntax:\n\t%s -A name password ipv6 [routedprefix/len]\n", argv[0]);
		return -1;
	}
				
	db_init ();

	if (db_load () == -1) {
		printf ("> ERROR -> Problem with loading " DEFAULT_DATABASE_PATH " file\n");
		return -1;
	}

	if (db_find_byname (argv[2])) {
		printf ("ERROR -> client %s already exists\n", argv[2]);
		return -1;
	}

	ipv6_addr_t ip;
	ipv6_prefix_t prefix;
	memset (ip, 0, sizeof (ipv6_addr_t));
	memset (prefix, 0, sizeof (ipv6_prefix_t));
				
	/* convert ipv6 ascii address to binary format */
	if (inet_pton (AF_INET6, argv[4], ip) != 1) {
		printf ("WARNING -> invalid IPv6 address\n");
		return -1;
	}

	/* check for routed prefix */
	if (argc == 6) {
		char *s = strchr (argv[5], '/');

		if (!s) {
			printf ("WARNING -> DB syntax error\n");
			return -1;
		}

		*s = '\0';

		/* convert ipv6 prefix ascii address to binary format */
		if (inet_pton (AF_INET6, argv[5], prefix) != 1) {
			printf ("WARNING -> invalid IPv6 prefix\n");
			return -1;
		}

		s ++;

		/* get and set prefix length into last byte */
		prefix[15] = (unsigned char) atoi (s);
	}
  
	char *pwd = base64 (argv[3], strlen (argv[3]));
  
	if (!pwd)
		return -1;
	
	db_add (ip, prefix, argv[2], pwd);

	free (pwd);
	
	if (db_save () == -1) {
		printf ("ERROR -> Problem with saving " DEFAULT_DATABASE_PATH " file\n");
		return -1;
	}

	return 0;
}

int admin_db_update (int argc, char **argv)
{
	if (argc != 5) {
		printf ("> ERROR -> Please enter required parameters, syntax:"
			"\n\t%s -U name p NEWPASSWORD"
			"\n\t%s -U name i NEWIPV6"
			"\n\t%s -U name r NEWPREFIX/LEN\n", argv[0], argv[0], argv[0]);
		return -1;
	}

	db_init ();

	if (db_load () == -1) {
		printf ("> ERROR -> Problem with loading " DEFAULT_DATABASE_PATH " file\n");
		return -1;
	}

	client_t *c = db_find_byname (argv[2]);

	if (!c) {
		printf ("ERROR -> Can't find client %s in DB\n", argv[2]);
		return -1;
	}
	
	ipv6_addr_t ip;
	ipv6_prefix_t prefix;
	memcpy (ip, c->ip, sizeof (ipv6_addr_t));
	memcpy (prefix, c->prefix, sizeof (ipv6_prefix_t));
	
	char *s;
	char *pwd = c->pwd;
	char *tmp = 0;
	
	switch (*argv[3]) {
		case 'p':
			tmp = base64 (argv[4], strlen (argv[4]));
			pwd = tmp;
			break;
		case 'i':
			/* convert ipv6 ascii address to binary format */
			if (inet_pton (AF_INET6, argv[4], ip) != 1) {
				printf ("WARNING -> invalid IPv6 address\n");
				return -1;
			}
			break;
		case 'r':
			if (!strcmp (argv[4], "-")) {
				memset (prefix, 0, sizeof (ipv6_prefix_t));
				break;
			}
		  
			/* parse ipv6 prefix with prefix length */
			s = strchr (argv[4], '/');

			if (!s) {
				printf ("WARNING -> DB syntax error\n");
				return -1;
			}

			*s = '\0';

			/* convert ipv6 prefix ascii address to binary format */
			if (inet_pton (AF_INET6, argv[4], prefix) != 1) {
				printf ("WARNING -> invalid IPv6 prefix\n");
				return -1;
			}

			s ++;

			/* get and set prefix length into last byte */
			prefix[15] = (unsigned char) atoi (s);
			
			break;
	}

	db_update (c, ip, prefix, pwd);

	if (tmp)
		free (tmp);
	
	if (db_save () == -1) {
		printf ("ERROR -> Problem with saving " DEFAULT_DATABASE_PATH " file\n");
		return -1;
	}

	return 0;
}

int admin_db_get (int argc, char **argv)
{
	if (argc != 3) {
		printf ("> ERROR -> Please enter required parameters, syntax:"
			"\n\t%s -G name\n", argv[0]);
		return -1;
	}

	db_init ();

	if (db_load () == -1) {
		printf ("> ERROR -> Problem with loading " DEFAULT_DATABASE_PATH " file\n");
		return -1;
	}

	client_t *c = db_find_byname (argv[2]);

	if (!c) {
		printf ("ERROR -> Can't find client %s in DB\n", argv[2]);
		return -1;
	}

	ipv6_prefix_t pr;
	memcpy (pr, c->prefix, sizeof (ipv6_prefix_t));
	unsigned char l = pr[15]; 
	pr[15] = 0;
	
	if (l) {
		char *prefix = strdup (ipv6_print ((unsigned short *) pr));
		
		if (!prefix)
			return -1;

		printf ("DB -> Client %s -> %s %s/%d\n", argv[2], ipv6_print (c->ip), prefix, l);
	
		free (prefix);
	} else
		printf ("DB -> Client %s -> %s -\n", argv[2], ipv6_print (c->ip));
	
	return 0;
}
