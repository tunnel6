/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _DB_H
#define _DB_H

#include <sys/time.h>
#include "ipv6.h"

/* Client structure */
typedef struct client_context {
	struct client_context *next, *prev;

	ipv6_addr_t ip;			/* IPv6 */
	ipv6_addr_t prefix;		/* routed prefix */
	char *name;			/* name */
	char *pwd;			/* password */
	struct sockaddr_in *info;	/* socket info */
	struct timeval tnow;		/* time of the last heartbeat */
} client_t;

typedef struct {
	int (*load) ();
	int (*save) ();
	int (*reload) ();
	int (*init) ();
} db_backend_t;

extern int db_add (ipv6_addr_t ip, ipv6_prefix_t prefix, char *name, char *pwd);
extern int db_update (client_t *c, ipv6_addr_t ip, ipv6_prefix_t prefix, char *pwd);
extern int db_disconnect_all ();
extern client_t *db_check (ipv6_addr_t ip);
extern client_t *db_find (struct sockaddr_in *info);
extern client_t *db_find_byname (char *name);
extern client_t *db_verify (char *name, char *pwd);
extern int db_load ();
extern int db_save ();
extern int db_flush (client_t *c);
extern int db_reload ();
extern int db_init ();

#endif
