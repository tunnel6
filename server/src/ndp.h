/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _NDP_H
#define _NDP_H

#include "ipv6.h"
#include "packet.h"

#define NDP_TYPE_NBSOL		135
#define NDP_TYPE_NBADV		136

struct proto_ndp_t {
	unsigned char type;
	unsigned char code;
	unsigned short checksum;
	unsigned flags;
	ipv6_addr_t target;
	
	/* ICMPv6 options */
	unsigned char ll_type;
	unsigned char ll_length;
	mac_addr_t ll_mac;
};

extern int ndp_reply (struct proto_eth_t *eth, char *data);

#endif
