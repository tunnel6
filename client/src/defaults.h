/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _DEFAULTS_H
#define _DEFAULTS_H

#define DEFAULT_VERSION		"1.0"

#define DEFAULT_ETHDEV		"eth0"
#define DEFAULT_PORT		6060

#define DEFAULT_MTU		1514
#define DEFAULT_BUF_SIZE	DEFAULT_MTU

#define DEFAULT_TUNTAP_DEV	"/dev/net/tun"
#define DEFAULT_TUNNEL6_DEV	"tunnel6"

#define DEFAULT_HEARTBEAT	5

#define DEFAULT_PIDFILE		"/var/run/tunnel6.pid"

#ifdef __WIN32__
# define DEFAULT_CONFIG_PATH	"client.txt"
#else
# define DEFAULT_CONFIG_PATH	"/etc/tunnel6/client.conf"
#endif

#endif
