<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cz" lang="cz">
<head>

<meta name="Description" content="Tunnel, IPv6, connectivity" />
<meta name="Keywords" content="tunnel, ipv6, connectivity" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Distribution" content="Global" />
<meta name="Author" content="Tomáš Jędrzejek" />
<meta name="Robots" content="index,follow" />

<link rel="stylesheet" href="images/CoolWater.css" type="text/css" />

<title>Poskytovatel Tunnel6</title>
	
</head>

<?php
require_once "config.php";
?>

<body>
<!-- wrap starts here -->
<div id="wrap">
		
	<!--header -->
	<div id="header">			
				
		<h1 id="logo-text"><a href="index.php">Tunnel6 server</a></h1>		
		<p id="slogan">by <?php echo "$provider"; ?></p>
			
	</div>
		
	<!-- navigation -->	
	<div  id="menu">
		<ul>
			<li id="current"><a href="index.php">Domů</a></li>
			<li><a href="reg.php">Registrace</a></li>
			<li><a href="http://tunnel6.zexos.org/">Tunnel6</a></li>	
		</ul>
	</div>					
			
	<!-- content-wrap starts here -->
	<div id="content-wrap">
		
		<div id="main">				
				
			<a name="Info"></a>
			<h2><a href="index.php">Poskytovatel Tunnel6</a></h2>
					
			<p class="post-by">napsal: <a href="index.php">ZeXx86</a></p>
				
            <p><strong><?php echo "$provider"; ?></strong> nabízí přístup všem k IPv6 internetu, kteří jej nemají nativně.<br />
	    Přinášíme vám alternativní řešení v podobě <a href="http://tunnel6.zexos.org">Tunnel6</a> technologie.
            </p>
	    
            <p>
            Tunnel6 používá protokol UDP, jež je umístěn nad IPv4 vrstvou, což umožňuje přenos IPv6 packetů.
	    Výhodou je, že projde skrz všechny NATované přípojky včetně běžných firewallů, to znamená, že <i>klient vůbec nepotřebuje veřejnou IPv4 adresu</i>. 
            </p>

            <p>
	    Jednoduše si <a href="reg.php">zaregisterujte</a> váš účet a užívejte si IPv6 kdekoliv !
            </p>

            <p>
	  
	    <?php 
		    $c = 0;
		    $data = File ($t6db);
		    for ($i = 0; $i < Count ($data); $i++) {
			  if (strlen ($data[$i]) > 3)
				if (!strstr ($data[$i], "#"))
					$c ++;
		    }

		    if ($c > 1)
			  echo "$provider právě poskytuje IPv6 tunel pro $c klientů, můžete být další..";
	    ?> 
	    
            </p>

		</div>
		
			
		<div id="sidebar">
			
			<h2>Menu</h2>
			<ul class="sidemenu">				
				<li><a href="http://tunnel6.zexos.org/#info">Co je Tunnel6</a></li>
				<li><a href="http://tunnel6.zexos.org/#works">Jak to funguje</a></li>
				<li><a href="http://tunnel6.zexos.org/#platforms">Podporované platformy</a></li>
			</ul>	
				
			<h2>Odkazy</h2>
			<ul class="sidemenu">
				<li><a href="http://www.ipv6portal.cz">IPv6 Portal</a></li>
				<li><a href="http://www.zexos.org">ZeX/OS</a></li>
				<li><a href="http://www.rd-hosting.eu">RD-Hosting</a></li>
			</ul>
			
			<h2>Podporujte IPv6</h2>
			<p><a href="http://cs.wikipedia.org/wiki/IPv6">IPv6</a> (internetový protokol verze 6) je v informatice označení nastupujícího protokolu pro komunikaci v současném Internetu (resp. v počítačových sítích, které Internet vytvářejí). IPv6 nahrazuje dosluhující protokol IPv4.</p>

			<p><form action="https://www.paypal.com/cgi-bin/webscr" method="post">
			<input type="hidden" name="cmd" value="_s-xclick">
			<input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----MIIHLwYJKoZIhvcNAQcEoIIHIDCCBxwCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYAvzl20Ki1vmOpNW5xNEh1IRYScf7agugOzNc8Ii/JnkcYgBJBRrJE9RVRpbZpjgyVwFcOWYYTWOkJ2c6FnVUYYU6WX3EyBBDoEes/UMtfi68l2aq8O0vbGk1C8oxfHl/zYiBDbWE2rffx/F/+UXZppGjJ2h76q+QOx5rMrgzXo+TELMAkGBSsOAwIaBQAwgawGCSqGSIb3DQEHATAUBggqhkiG9w0DBwQI88JhWHMcGW6AgYjMxV3MlAStqQwhINIj93+gw7cKam2VHM4yw5gnurr1z3yeIpoQy5OXMfoc277sXo+95QA/1q3VxXdeCK0Raz+C4cpysqqkZfJgAKFNEn0vZTzjgs+f/UZjRQLq1r6m+lvgQvpRgUivTlIx505Cwh52QVTH6e0acIW+AVssrSl0P+xBz9g9L1qRoIIDhzCCA4MwggLsoAMCAQICAQAwDQYJKoZIhvcNAQEFBQAwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMB4XDTA0MDIxMzEwMTMxNVoXDTM1MDIxMzEwMTMxNVowgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDBR07d/ETMS1ycjtkpkvjXZe9k+6CieLuLsPumsJ7QC1odNz3sJiCbs2wC0nLE0uLGaEtXynIgRqIddYCHx88pb5HTXv4SZeuv0Rqq4+axW9PLAAATU8w04qqjaSXgbGLP3NmohqM6bV9kZZwZLR/klDaQGo1u9uDb9lr4Yn+rBQIDAQABo4HuMIHrMB0GA1UdDgQWBBSWn3y7xm8XvVk/UtcKG+wQ1mSUazCBuwYDVR0jBIGzMIGwgBSWn3y7xm8XvVk/UtcKG+wQ1mSUa6GBlKSBkTCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb22CAQAwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQUFAAOBgQCBXzpWmoBa5e9fo6ujionW1hUhPkOBakTr3YCDjbYfvJEiv/2P+IobhOGJr85+XHhN0v4gUkEDI8r2/rNk1m0GA8HKddvTjyGw/XqXa+LSTlDYkqI8OwR8GEYj4efEtcRpRYBxV8KxAW93YDWzFGvruKnnLbDAF6VR5w/cCMn5hzGCAZowggGWAgEBMIGUMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbQIBADAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMDkwMTE2MjEzNTQwWjAjBgkqhkiG9w0BCQQxFgQU9HhvbrzAoT1gGXgKR0Joe4D2VxQwDQYJKoZIhvcNAQEBBQAEgYALZEwnlDHV9TDjizfeqF6UtJAKMFbfr719hjLBFqMHtmnQPJzz6URllIgI8N+JMLO9Ijw6cinAL9tQEGamhDi1WJwojahLo3s+0meRCs5FEgQPsXQp/j73bDhz4He97uv4HLNTRhgus3ZIGnamkLW2KeajUJR2bNz22UtLs10D1A==-----END PKCS7-----
			">
			<input type="image" src="https://www.paypal.com/en_GB/i/btn/btn_donate_LG.gif" border="0" name="submit" alt="">
			</form></p>
		</div>
				
	<!-- content-wrap ends here -->	
	</div>
					
	<!--footer starts here-->
	<div id="footer">	
		<p>
		&copy; 2010 <strong>ZeXx86</strong> |
		<a href="http://www.bluewebtemplates.com/" title="Website Templates">website templates</a> by <a href="http://www.styleshout.com/">styleshout</a> |
		Valid <a href="http://validator.w3.org/check?uri=referer">XHTML</a> | 
		<a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a>   		
   	</p>
				
	</div>	

<!-- wrap ends here -->
</div>

</body>
</html>
