/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <mysql/mysql.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "db.h"
#include "ipv6.h"
#include "proto.h"
#include "config.h"
#include "base64.h"
#include "defaults.h"

#define STR_SQL_SIZE		4096

extern client_t client_list;
extern config_t config;

typedef struct db_sql_struct {
	MYSQL mysql;
	MYSQL_RES *res;
	MYSQL_FIELD *field;
	MYSQL_ROW row;
} sql_t;
sql_t *sql;

static char sql_cmd[STR_SQL_SIZE];

static sql_t *sql_connect (char *host, char *name, char *pwd, char *db)
{
	sql_t *new = calloc (1, sizeof(sql_t));

	if (!new)
		return 0;

        mysql_init (&(new->mysql));

        if (!mysql_real_connect (&(new->mysql), host, name, pwd, db, 0, NULL, 0)) {
		printf ("DB backend -> %s\n", mysql_error (&(new->mysql)));
		free (new);
		return 0;
	}

	return new;
}

static int sql_cmd_send (char *cmd)
{
	if (!sql)
		return -1;

	if (mysql_query (&(sql->mysql), cmd)) {
		printf ("DB backend -> %s\n", mysql_error (&(sql->mysql)));
		return -1;
        }

	return 0;
}

static int sql_data_get ()
{
	sql->res = mysql_store_result (&(sql->mysql) );

	if (!sql->res)
		return -1;

	return mysql_num_rows (sql->res);
}

static char *sql_datafield_get (int i)
{
	if (!sql->row)
		return 0;

	return sql->row[i];
}

static void sql_data_free ()
{
	mysql_free_result (sql->res);
	sql->res = NULL;
}

static int sql_field_count ()
{
	if (!sql->res)
		return -1;

	sql->row = mysql_fetch_row (sql->res);

	if (!sql->row)
		return -1;

	return mysql_field_count (&(sql->mysql));
}

/* load database of clients from file */
static int db_mysql_load ()
{
	ipv6_addr_t cl;
	ipv6_prefix_t pr;

	snprintf (sql_cmd, STR_SQL_SIZE, "SELECT * FROM `client_list`");

	if (sql_cmd_send (sql_cmd) < 0)
		return -1;
	
	int ret = sql_data_get ();

	if (ret <= 0) {
		printf ("NOTICE -> no data in database\n");
		return 0;
	}
		
	for (;;) {
		int count = sql_field_count ();

		if (count != 4)
			break;

		char *name = sql_datafield_get (0);
		char *pwd = sql_datafield_get (1);
		char *ipv6 = sql_datafield_get (2);
		char *prefix = sql_datafield_get (3);

		if (!name || !pwd || !ipv6 || !prefix)
			break;

		if (inet_pton (AF_INET6, ipv6, cl) != 1) {
			printf ("WARNING -> DB syntax error -> client %s : [%s]\n", name, ipv6);
			continue;
		}

		if (strcmp (prefix, "-")) {
			char *s = strchr (prefix, '/');
			
			if (!s) {
				printf ("WARNING -> DB syntax error -> client %s : [%s]\n", name, prefix);
				continue;
			}
			
			*s = '\0';
	
			if (inet_pton (AF_INET6, prefix, pr) != 1) {
				printf ("WARNING -> DB syntax error -> client %s : [%s]\n", name, prefix);
				continue;
			}
			
			s ++;
	
			pr[15] = (unsigned char) atoi (s);
		} else
			memset (pr, 0x0, sizeof (ipv6_prefix_t));
		
		client_t *c = db_find_byname (name);

		if (c) {
		  	db_update (c, cl, pr, pwd);
			continue;
		}
		
		db_add (cl, pr, name, pwd);
	}
	
	sql_data_free ();

	return 0;
}

/* save database of clients to mysql */
static int db_mysql_save ()
{
	char ipv6[80];
	char prefix[80];
	ipv6_prefix_t pr;

	client_t *client;
	for (client = client_list.next; client != &client_list; client = client->next) {
		snprintf (sql_cmd, STR_SQL_SIZE, "SELECT * FROM  `client_list` WHERE  `name` = '%s'", client->name);

		if (sql_cmd_send (sql_cmd) < 0)
			return 0;
		
		int ret = sql_data_get ();

		/* record does'nt exist, add it */
		if (ret <= 0) {
			inet_ntop (AF_INET6, client->ip, ipv6, 80);

			memcpy (pr, client->prefix, sizeof (ipv6_prefix_t));
			
			if (pr[15]) {
				unsigned char l = pr[15];
				pr[15] = 0;
			  
				inet_ntop (AF_INET6, pr, prefix, 80);
				
				snprintf (sql_cmd, STR_SQL_SIZE, "INSERT INTO  `client_list` (`name` ,`pwd` ,`ipv6` ,`prefix`)"
								"VALUES ('%s', '%s', '%s', '%s/%d')", client->name, client->pwd, ipv6, prefix, l);
				
			} else
				snprintf (sql_cmd, STR_SQL_SIZE, "INSERT INTO  `client_list` (`name` ,`pwd` ,`ipv6` ,`prefix`)"
								"VALUES ('%s', '%s', '%s', '-')", client->name, client->pwd, ipv6);

			if (sql_cmd_send (sql_cmd) < 0)
				return -1;
		} else { /* update records */
			int count = sql_field_count ();

			if (count != 4)
				break;

			char *pwd = sql_datafield_get (1);
			char *ip = sql_datafield_get (2);
			char *pref = sql_datafield_get (3);

			if (!pwd || !ip || !pref)
				break;
			
			/* check password */
			if (strcmp (pwd, client->pwd)) {
				snprintf (sql_cmd, STR_SQL_SIZE, "UPDATE `client_list` SET `pwd` = '%s' WHERE CONVERT( `client_list`.`name` USING utf8 ) = '%s'", client->pwd, client->name);

				if (sql_cmd_send (sql_cmd) < 0)
					return -1;
			}

			/* check ipv6 */
			inet_ntop (AF_INET6, client->ip, ipv6, 80);

			if (strcmp (ip, ipv6)) {
				snprintf (sql_cmd, STR_SQL_SIZE, "UPDATE `client_list` SET `ipv6` = '%s' WHERE CONVERT( `client_list`.`name` USING utf8 ) = '%s'", ipv6, client->name);

				if (sql_cmd_send (sql_cmd) < 0)
					return -1;
			}

			/* check prefix */
			memcpy (pr, client->prefix, sizeof (ipv6_prefix_t));

			char tmp[80];
			
			if (pr[15]) {
				unsigned char l = pr[15];
				pr[15] = 0;
			  
				inet_ntop (AF_INET6, pr, prefix, 80);
				
				sprintf (tmp, "%s/%d", prefix, l);
			} else
				strcpy (tmp, "-");
			
			if (strcmp (tmp, pref)) {
				snprintf (sql_cmd, STR_SQL_SIZE, "UPDATE `client_list` SET `prefix` = '%s' WHERE CONVERT( `client_list`.`name` USING utf8 ) = '%s'", tmp, client->name);

				if (sql_cmd_send (sql_cmd) < 0)
					return -1;
			}
		}
	}

	return 0;
}

/* reload database on demand */
static int db_mysql_reload ()
{
	/* (re)load database file */
	if (db_mysql_load () == -1) {
		printf ("ERROR -> Database file '%s' is missing !\n\tSyntax per line: name password clientipv6 -/routedprefix\n", DEFAULT_DATABASE_PATH);
		return -1;
	}
	
	return 0;
}

static int db_mysql_init ()
{
	if (!config.mysql_host || !config.mysql_user || !config.mysql_pwd || !config.mysql_db) {
		printf ("DB backend -> please set MySQL host, user, password and database in config " DEFAULT_CONFIG_PATH "\n");
		return -1;
	}
  
	sql = sql_connect (config.mysql_host, config.mysql_user, config.mysql_pwd, config.mysql_db);

	if (sql)
		printf ("DB backend -> succesfully connected to MySQL database\n");
	else {
		printf ("DB backend -> connection to MySQL database failed\n");
		return -1;
	}

	/* create table in DB */
	snprintf (sql_cmd, STR_SQL_SIZE, "CREATE TABLE IF NOT EXISTS `client_list` ("
					"`name` varchar(32) NOT NULL,"
					"`pwd` varchar(80) NOT NULL,"
					"`ipv6` varchar(40) NOT NULL,"
					"`prefix` varchar(45) NOT NULL,"
					"PRIMARY KEY  (`name`)"
					") ENGINE=MyISAM DEFAULT CHARSET=utf8;");

	if (sql_cmd_send (sql_cmd) < 0)
		return -1;

	return 0;
}

db_backend_t db_mysql = {
	.load = db_mysql_load,
	.save = db_mysql_save,
	.reload = db_mysql_reload,
	.init = db_mysql_init,
};