/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __WIN32__
# include <sys/socket.h>
# include <sys/types.h>
# include <netinet/in.h>
# include <arpa/inet.h>
# include <fcntl.h>
# include <unistd.h>
# include <string.h>
# include <stdlib.h>
# include <netdb.h>
#else
# include <winsock2.h>
# include <ws2tcpip.h>
#endif
#include <stdio.h>
#include "ipv6.h"
#include "proto.h"
#include "packet.h"
#include "defaults.h"

#ifndef __WIN32__
static int fd;				/* File descriptor of the current session */
extern int fd_udp;			/* Polling socket */
#else
static SOCKET fd;			/* File descriptor of the current session */
extern SOCKET fd_udp;			/* Polling socket */
#endif
static char data[DEFAULT_MTU+1];	/* Receiver data buffer */

/* receive data from tunnel server */
int tunnel_recv ()
{  
	int ret = recv (fd, data, DEFAULT_MTU, 0);

	if (ret < 1) {
		//printf ("ERROR -> recvfrom ()\n");
		return 0;
	}
	
	data[ret] = '\0';
	
	return ret;
}

/* check for new data from tunnel server */
int tunnel_poll ()
{
	int ret = tunnel_recv ();
	
	if (!ret)
		return 0;

	/* parse received data */
	return proto_parse (data, ret);
}

/* send data to tunnel server */
int tunnel_send (char *packet, unsigned len)
{
        if (send (fd, packet, len, 0) == -1) {
		printf ("ERROR -> send ()\n");
		return -1;
	}
	
	return 0;
}

/* try to connect to tunnel server and log in */
int tunnel_init (char *addr, unsigned short port, char *name, char *pwd)
{
#ifdef __WIN32__
	/* initialize WinSock
	   version of WinSock API (should be supported from W. XP) */
	WORD wVersionRequested = MAKEWORD (2, 2);
	WSADATA wsadata;
	
	if (WSAStartup (wVersionRequested, &wsadata)) {
	    return -1;
	}
#endif
	/* obtain address(es) matching host/port */
	struct addrinfo hints;
	struct addrinfo *result, *rp;

	memset (&hints, 0, sizeof (struct addrinfo));
	hints.ai_family = AF_INET;    		/* allow IPv4 */
	hints.ai_socktype = SOCK_DGRAM; 	/* datagram socket */
	hints.ai_protocol = IPPROTO_UDP;	/* UDP protocol */

	char sport[8];
	sprintf (sport, "%d", port);		/* convert unsigned short to string */

	int s = getaddrinfo (addr, sport, &hints, &result);

	if (s) {
		printf ("ERROR -> getaddrinfo ()\n");

		goto err;
	}

	/* getaddrinfo () returns a list of address structures.
	   Try each address until we successfully connect().
	   If socket () (or connect ()) fails, we (close the socket
	   and) try the next address. */

	for (rp = result; rp != NULL; rp = rp->ai_next) {
		fd = socket (rp->ai_family, rp->ai_socktype, rp->ai_protocol);

		if (fd == -1)
			continue;

		if (connect (fd, rp->ai_addr, rp->ai_addrlen) != -1)
			break; /* Success */
#ifndef __WIN32__
		close (fd);
#else
		closesocket (fd);
#endif
	}

	if (rp == NULL) {               /* No address succeeded */
		printf ("ERROR -> Unable to connect to '%s'\n", addr);

		goto err;
	}

	freeaddrinfo (result);           /* No longer needed */

	/* send login request to server */
	if (proto_login (data, name, pwd) == -1)
		goto err;
	
	/* save polling socket */
	fd_udp = fd;

	return 0;
	
err:	/* error */
#ifdef __WIN32__
	WSACleanup();
#endif
	return -1;
}
