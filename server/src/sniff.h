/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _SNIFF_H
#define _SNIFF_H

#include <net/if.h>
#include <netinet/in.h>
#include <linux/if_packet.h>
#include "ipv6.h"
#include "packet.h"
#include "defaults.h"

struct netdev_t {
	int fd;		/* eth sniffing socket */
	int fd_raw;	/* RAW eth sender socket */
	int fd_raw2;	/* IPv6 eth sender socket */
	int fd_tun;	/* tunnel socket */
	char buf[DEFAULT_BUF_SIZE+1];
	ipv6_addr_t ip;
	mac_addr_t hwaddr;
	int ifindex;
	struct sockaddr_ll s_raw;
};

extern int sniff_recv ();
extern int sniff_send (char *packet, int packet_size);
extern int sniff_send_ipv6 (char *packet, int packet_size);
extern int sniff_init (char *dev, char *listenipv6);

#endif
