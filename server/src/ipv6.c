/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include "db.h"
#include "ndp.h"
#include "ipv6.h"
#include "tunnel.h"
#include "packet.h"
#include "defaults.h"

static char ipv6[64];			/* temporary buffer used by ipv6_print () */

/* compare two IPv6 address */
unsigned ipv6_cmp (ipv6_addr_t ip, ipv6_addr_t ip2)
{
	unsigned i;
	for (i = 0; i < 8; i ++)
		if (ip[i] != ip2[i])
			return 0;

	return 1;
}

/* compare two IPv6 prefix address */
unsigned ipv6_cmp_prefix (ipv6_prefix_t pr, ipv6_prefix_t ip)
{
	if (!pr[15])
		return 0;

	unsigned i;
	for (i = 0; i < (pr[15]/8); i ++)
		if (pr[i] != ip[i])
			return 0;
		
	return 1;
}

/* print IPv6 address into buffer */
char *ipv6_print (ipv6_addr_t ip)
{
	if (!inet_ntop (AF_INET6, ip, ipv6, 64))
		return 0;
	
	return ipv6;
}

/* parse incoming data - native IPv6 */
int ipv6_parse (struct proto_eth_t *eth, char *data, unsigned len)
{
	struct proto_ipv6_t *ip = (struct proto_ipv6_t *) data;
	
	/* its NDP packet */
	if (ip->nhead == IPV6_TYPE_ICMPv6)
		if (ndp_reply (eth, data) != -1)
			return 0;
		
	/* its TCP/UDP or ICMP packet */
	client_t *c = db_check (ip->dest);

	if (!c)
		return 0;

	if (!c->info)
		return 0;

	/* send data to client over IPv4 tunnel */
	return tunnel_send (c, data, len);
}

int ipv6_init ()
{

	return 0;
}
