<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<meta name="Description" content="Tunnel, IPv6, connectivity" />
<meta name="Keywords" content="tunnel, ipv6, connectivity" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Distribution" content="Global" />
<meta name="Author" content="Tomáš Jędrzejek" />
<meta name="Robots" content="index,follow" />

<link rel="stylesheet" href="images/CoolWater.css" type="text/css" />

<title>Tunnel6 provider</title>
	
</head>

<?php
require_once "config.php";
?>

<body>
<!-- wrap starts here -->
<div id="wrap">
		
	<!--header -->
	<div id="header">			
				
		<h1 id="logo-text"><a href="index.php">Tunnel6 server</a></h1>		
		<p id="slogan">by <?php echo "$provider"; ?></p>
			
	</div>
		
	<!-- navigation -->	
	<div  id="menu">
		<ul>
			<li><a href="index.php">Home</a></li>
			<li id="current"><a href="reg.php">Register</a></li>
			<li><a href="http://tunnel6.zexos.org/">Tunnel6</a></li>	
		</ul>
	</div>					
			
	<!-- content-wrap starts here -->
	<div id="content-wrap">
		
		<div id="main">				
				
			<a name="Info"></a>
			<h2><a href="index.php">Registration of the Tunnel6 account</a></h2>
					
			<p class="post-by">posted by: <a href="index.php">ZeXx86</a></p>
				
<?
		if (!strlen ($_POST['login']) || !strlen ($_POST['pwd'])) {
?>
			<form action="reg.php" method="post" id="login">		
								
				<p>	
					<label for="login">Name</label><br />
					<input id="login" name="login" value="" type="text" tabindex="1" />
				</p>
			
				<p>
					<label for="pwd">Password</label><br />
					<input id="pwd" name="pwd" value="" type="password" tabindex="2" />
				</p>

				<p>
					<label for="captcha">Captcha (<?php echo $captchaq ?>)</label><br />
					<input id="captcha" name="captcha" value="" type="text" tabindex="3" />
				</p>

				<p class="no-border">
					<input class="button" type="submit" value="Register" tabindex="4" />         		
				</p>
					
			</form>

<?php
		} else {
			if (strlen ($_POST['login']) < 3 || strlen ($_POST['pwd']) < 3 || strlen ($_POST['captcha']) < 1 || $_POST['captcha'] != $captchaa) {
					echo "<p><strong>Login name, password or captcha are invalid, please try it again with correct values</strong><br><a href=\"reg.php\">Go back</a></p>";
			} else {
				$data = file ($t6db);
				$y = 0;

				for ($i = 0; $i < count ($data); $i++) {
					sscanf ($data[$i], "%s %s %s %s", $name, $password, $ipv6, $prefix);

					if ($name == $_POST['login']) {
						$y ++;
						break;
					}
				}

				if ($y) {
					echo "<p><strong>Login name you selected is already used, please try it again with another one !</strong><br><a href=\"reg.php\">Go back</a></p>";
				} else {
					if (!file_exists ($idfile)) {
						$file = fopen ($idfile, "a");
						fputs ($file, "1");
						fclose ($file);
						$id = 1;
					} else {
						$file = fopen ($idfile, "r+");
						$id = fgets ($file, 100);
						$id ++;
						fseek ($file, 0);
						fputs ($file, $id);
						fclose ($file);
					}

					$newipv6 = $routedprefix . $id;

					$fp = fopen ($t6db, "a");
					if ($fp) {
					      fwrite ($fp, $_POST['login']." ".$_POST['pwd']." ".$newipv6." -\n");
					      fclose ($fp);
					}

					echo "<p><strong>You are succefully registered !</strong></p>";
					echo "<p>You can configure your <a href=\"http://tunnel6.zexos.org/download.php\">Tunnel6 client</a> now</p>";
					echo "<p>Your tunnel6 config should contain fallowing lines:<code>name ".$_POST['login']."<br>password ".$_POST['pwd']."<br>server $server</code></p>";
					echo "<p><i>Please save the above login data ..</i></p>";
				}
			}
		}
?>

		</div>
		
			
		<div id="sidebar">
			
			<h2>Sidebar Menu</h2>
			<ul class="sidemenu">				
				<li><a href="http://tunnel6.zexos.org/#info">What is Tunnel6</a></li>
				<li><a href="http://tunnel6.zexos.org/#works">How it works</a></li>
				<li><a href="http://tunnel6.zexos.org/#platforms">Supported platforms</a></li>
			</ul>	
				
			<h2>Links</h2>
			<ul class="sidemenu">
				<li><a href="http://www.ipv6portal.cz">IPv6 Portal</a></li>
				<li><a href="http://www.zexos.org">ZeX/OS</a></li>
				<li><a href="http://www.rd-hosting.eu">RD-Hosting</a></li>
			</ul>
			
			<h2>Support IPv6</h2>
			<p><a href="http://en.wikipedia.org/wiki/IPv6">Internet Protocol version 6</a> (IPv6) is an Internet Protocol version which will succeed IPv4, the first implementation which is still in dominant use currently</p>

			<p><form action="https://www.paypal.com/cgi-bin/webscr" method="post">
			<input type="hidden" name="cmd" value="_s-xclick">
			<input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----MIIHLwYJKoZIhvcNAQcEoIIHIDCCBxwCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYAvzl20Ki1vmOpNW5xNEh1IRYScf7agugOzNc8Ii/JnkcYgBJBRrJE9RVRpbZpjgyVwFcOWYYTWOkJ2c6FnVUYYU6WX3EyBBDoEes/UMtfi68l2aq8O0vbGk1C8oxfHl/zYiBDbWE2rffx/F/+UXZppGjJ2h76q+QOx5rMrgzXo+TELMAkGBSsOAwIaBQAwgawGCSqGSIb3DQEHATAUBggqhkiG9w0DBwQI88JhWHMcGW6AgYjMxV3MlAStqQwhINIj93+gw7cKam2VHM4yw5gnurr1z3yeIpoQy5OXMfoc277sXo+95QA/1q3VxXdeCK0Raz+C4cpysqqkZfJgAKFNEn0vZTzjgs+f/UZjRQLq1r6m+lvgQvpRgUivTlIx505Cwh52QVTH6e0acIW+AVssrSl0P+xBz9g9L1qRoIIDhzCCA4MwggLsoAMCAQICAQAwDQYJKoZIhvcNAQEFBQAwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMB4XDTA0MDIxMzEwMTMxNVoXDTM1MDIxMzEwMTMxNVowgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDBR07d/ETMS1ycjtkpkvjXZe9k+6CieLuLsPumsJ7QC1odNz3sJiCbs2wC0nLE0uLGaEtXynIgRqIddYCHx88pb5HTXv4SZeuv0Rqq4+axW9PLAAATU8w04qqjaSXgbGLP3NmohqM6bV9kZZwZLR/klDaQGo1u9uDb9lr4Yn+rBQIDAQABo4HuMIHrMB0GA1UdDgQWBBSWn3y7xm8XvVk/UtcKG+wQ1mSUazCBuwYDVR0jBIGzMIGwgBSWn3y7xm8XvVk/UtcKG+wQ1mSUa6GBlKSBkTCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb22CAQAwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQUFAAOBgQCBXzpWmoBa5e9fo6ujionW1hUhPkOBakTr3YCDjbYfvJEiv/2P+IobhOGJr85+XHhN0v4gUkEDI8r2/rNk1m0GA8HKddvTjyGw/XqXa+LSTlDYkqI8OwR8GEYj4efEtcRpRYBxV8KxAW93YDWzFGvruKnnLbDAF6VR5w/cCMn5hzGCAZowggGWAgEBMIGUMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbQIBADAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMDkwMTE2MjEzNTQwWjAjBgkqhkiG9w0BCQQxFgQU9HhvbrzAoT1gGXgKR0Joe4D2VxQwDQYJKoZIhvcNAQEBBQAEgYALZEwnlDHV9TDjizfeqF6UtJAKMFbfr719hjLBFqMHtmnQPJzz6URllIgI8N+JMLO9Ijw6cinAL9tQEGamhDi1WJwojahLo3s+0meRCs5FEgQPsXQp/j73bDhz4He97uv4HLNTRhgus3ZIGnamkLW2KeajUJR2bNz22UtLs10D1A==-----END PKCS7-----
			">
			<input type="image" src="https://www.paypal.com/en_GB/i/btn/btn_donate_LG.gif" border="0" name="submit" alt="">
			</form></p>
		</div>
				
	<!-- content-wrap ends here -->	
	</div>
					
	<!--footer starts here-->
	<div id="footer">	
		<p>
		&copy; 2010 <strong>ZeXx86</strong> |
		<a href="http://www.bluewebtemplates.com/" title="Website Templates">website templates</a> by <a href="http://www.styleshout.com/">styleshout</a> |
		Valid <a href="http://validator.w3.org/check?uri=referer">XHTML</a> | 
		<a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a>   		
   	</p>
				
	</div>	

<!-- wrap ends here -->
</div>

</body>
</html>
