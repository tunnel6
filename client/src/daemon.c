/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/stat.h>

int daemonize (char *pidfile)
{
	int i, lockfd;
  	char pid[16];

	int ipid = getpid ();

  	/* detach if asked */
  	if (ipid == 1)
		return -1;			/* already a daemon */

    	/* fork to guarantee we are not process group leader */
    	i = fork ();
    	if (i < 0)
      		exit (1);			/* fork error */
    	if (i > 0)
      		exit (0);			/* parent exits */

    	/* child (daemon) continues */
    	setsid ();				/* obtain a new process group */
	ipid = getpid ()+1;

	printf ("> starting as daemon with pid -> %d\n", ipid);
    	/* fork again so we become process group leader 
     	 * and cannot regain a controlling tty 
     	 */
    	i = fork ();
    	if (i < 0)
      		exit (1);			/* fork error */
    	if (i > 0)
		exit (0);			/* parent exits */

    	/* close all fds */
    	for (i = getdtablesize (); i >= 0; --i)
      		close (i);			/* close all descriptors */

    	/* close parent fds and send output to fds 0, 1 and 2 to bitbucket */
    	i = open ("/dev/null", O_RDWR);

    	if (i < 0)
      		exit (1);
	
    	if (dup (i) == -1)
		return -1;
      	if (dup (i) == -1)			/* handle standart I/O */
		return -1;

  	/* create local lock */
  	lockfd = open (pidfile, O_RDWR | O_CREAT, 0640);
  	if (lockfd < 0) {
    		perror ("lock: open");
    		exit (1);
  	}

	/* lock the file */
	if (lockf (lockfd, F_TLOCK, 0) < 0) {
		perror ("lock: lockf");
		printf ("> ERROR -> Tunnel6 is already running\n");
		return -1;
	}
	
	/* write to pid to lockfile */
	snprintf (pid, 16, "%d\n", getpid ());
	if (write (lockfd, pid, strlen (pid)) == -1)
		return -1;

	/* restrict created files to 0750 */
	umask (027);
	
	return 0;
}
