/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <getopt.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "sig.h"
#include "ipv6.h"
#include "poll.h"
#include "proto.h"
#include "config.h"
#include "tunnel.h"
#include "tundev.h"
#include "daemon.h"
#include "defaults.h"
#include "heartbeat.h"
#ifdef __WIN32__
# include <windows.h>
#endif

int cont;			/* Continue in tunnel processing ? 1 - yes / 0 - no */
static char *name;		/* login name */
static char *pwd;		/* login password */
static char *server;		/* tunnel server address */
static unsigned short port;	/* listen port */
static char *pidfile;		/* pid file */
static char isdaemon;

int init ()
{
	if (tundev_init () == -1)
		return -1;

	if (tunnel_init (server, port, name, pwd) == -1)
		return -1;

	if (ipv6_init () == -1)
		return -1;

	if (tundev_setup () == -1)
		return -1;

	if (heartbeat_init () == -1)
		return -1;

	if (poll_init () == -1)
		return -1;

	if (sig_init () == -1)
		return -1;

	cont = 1;

	return 0;
}

int loop ()
{
	/* loop until cont is non-zero */
	for (; cont;) {
		poll_loop ();		/* check socket events */
		
		heartbeat_loop ();	/* send heartbeat packets in interval */
	}
	
	return 0;
}

void usage ()
{
	printf ("TUNNEL6 client\nUsage:\n"
		"\t-h : help\n"
		"\t-v : version\n"
#ifndef __WIN32__
		"\t-d : start as daemon\n"
		"\t-f : specify pid file\n"
#endif
		"\t-p : specify port\n"
		"\t-P : specify password\n"
		"\t-n : specify login name\n"
		"\t-s : specify server\n"
#ifndef __WIN32__
		"\t-l : remap stdout to specified file\n"
#endif
		);
}

void parseargs (int argc, char **argv)
{
	int opt;

 	while ((opt = getopt (argc, argv, "hvdp:f:s:P:n:l:")) > 0) {
		switch (opt) {
			case '?':
			case 'h':
				usage ();
				exit (0);
			case 'v':
				printf ("TUNNEL6 client - version " DEFAULT_VERSION "\n");
				exit (0);
			case 'p':
				port = (unsigned short) atoi (optarg);
				break;
			case 's':
				server = strdup (optarg);
				break;
			case 'P':
				pwd = strdup (optarg);
				break;
			case 'n':
				name = strdup (optarg);
				break;
#ifndef __WIN32__
			case 'd':
				isdaemon = 1;
				break;
			case 'f':
				pidfile = strdup (optarg);
				break;
			case 'l':
				stdout = fopen (optarg, "a+");
				
				if (!stdout) {
					perror ("> ERROR -> Log file");
					exit (-1);
				}

				break;
#endif
		}
	}
}

int main (int argc, char **argv)
{
	isdaemon = 0;
	name = 0;
	pwd = 0;
	server = 0;
	port = DEFAULT_PORT;
	pidfile = DEFAULT_PIDFILE;
	
	/* we've found config */
	if (config_init () != -1) {
		/* set config values */
		name = config.name;
		pwd = config.pwd;
		server = config.server;
		
		if (config.port)
			port = config.port;
	}
	
	parseargs (argc, argv);
	
	/* unbuffer stdout */
	setvbuf (stdout, (char *) NULL, _IOLBF, 0);
	
	printf ("TUNNEL6 client v%s by ZeXx86\n", DEFAULT_VERSION);
	
	if (!name || !server || !port) {
		printf ("> ERROR -> Please set name, password and server !\n\tCheck config file or parameters\n");
		return -1;
	}

#ifndef __WIN32__
	if (isdaemon)
		daemonize (pidfile);
#endif

	printf ("User name: %s\n"
		"Server: %s\n"
		"Port: %u\n", name, server, port);

	/* Initialize routines */
	if (init () == -1)
		return -1;

	loop ();

	printf ("> closing tunnel\n");
  
	/* disconnect from server */
	proto_cmd_disconnect2 ();

	return 0;
}
