/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __WIN32__
#include <stdlib.h>
#include <string.h>
# include <netinet/in.h>
#else
# include <windows.h>
#endif
#include <stdio.h>
#include "ipv6.h"
#include "proto.h"
#include "base64.h"
#include "tundev.h"
#include "tunnel.h"
#include "packet.h"
#include "defaults.h"

extern int cont;			/* Continue in tunnel processing ? 1 - yes / 0 - no */
ipv6_addr_t tunip;			/* IPv6 address obtained from tunnel server */
ipv6_addr_t tungw;			/* IPv6 gateway obtained from tunnel server */
ipv6_prefix_t tunpr;			/* IPv6 prefix obtained from tunnel server */

/* PROTOCOL Command - LOGIN */
int proto_login (char *data, char *name, char *pwd)
{
	/* encode password */
	char *pwd_new = base64 (pwd, strlen (pwd));
  
	if (!pwd_new)
		return -1;
	
	unsigned name_len = strlen (name);
	unsigned pwd_len = strlen (pwd_new);

	char *req = (char *) malloc (5 + name_len + pwd_len);
	
	if (!req)
		return -1;
	
	/* fill the protocol command */	
	req[0] = PROTO_MAGIC;		/* magic signature */
	req[1] = PROTO_CMD_LOGIN;	/* login cmd */
	req[2] = name_len;
	req[3] = pwd_len;
	
	memcpy (req+4, name, name_len);
	memcpy (req+4+name_len, pwd_new, pwd_len);
	
	free (pwd_new);
	
	/* send LOGIN request to tunnel server */
	if (tunnel_send (req, 4 + name_len + pwd_len) == -1) {
		printf ("ERROR -> tunnel_send ()\n");
		free (req);
		return -1;
	}
	
	free (req);
	
	/* Receive protocol data */
	if (tunnel_recv () == 0) {
		printf ("ERROR -> tunnel_recv ()\n");
		return -1;
	}

	/* Was a login succesfull ? */
	switch (data[0]) {
		case PROTO_CMD_LOGIN_OK:
			break;
		case PROTO_CMD_LOGIN_FAIL:
			printf ("ERROR -> Login failed - incorrect name or password !\n");
			return -1;
		case PROTO_CMD_LOGIN_MULTI:
			printf ("ERROR -> Login failed - you are logged multiply times !\n");
			return -1;
	}
	
	/* save obtained IPv6 address */
	memcpy (tunip, data+1, sizeof (ipv6_addr_t));
	/* save obtained IPv6 gateway */
	memcpy (tungw, data+1+sizeof (ipv6_addr_t), sizeof (ipv6_addr_t));
	/* save obtained IPv6 prefix for routing */
	memcpy (tunpr, data+1+2*sizeof (ipv6_prefix_t), sizeof (ipv6_prefix_t));

	printf ("> Login succesfull !\n> Obtained IPv6: %s\n", ipv6_print (tunip));
	
	/* Display routed prefix when available */
	if (tunpr[15]) {
		ipv6_addr_t tmp;
		memcpy (tmp, (void *) tunpr, sizeof (ipv6_addr_t)-1);
		memset ((char *) tmp + 15, 0, 1);
		
		printf ("> Routed IPv6 prefix: %s/%u\n", ipv6_print (tmp), (unsigned char) tunpr[15]);
	}
		
	return 0;
}

/* PROTOCOL Command - DISCONNECT by server*/
int proto_cmd_disconnect ()
{
	printf ("> disconnected by server\n");

	cont = 0;
	
	return 0;
}

/* PROTOCOL Command - DISCONNECT */
int proto_cmd_disconnect2 ()
{
	printf ("> disconnected\n");

	/* build command */
	char req[2];
	req[0] = PROTO_MAGIC;
	req[1] = PROTO_CMD_DISCONNECT;

	return tunnel_send (req, 2);
}

/* TUNNEL PROTOCOL HANDLER */
int proto_cmd (char *data, unsigned len)
{
	char *proto = (char *) data;
	
	switch (*proto) {
		case PROTO_CMD_DISCONNECT:
			return proto_cmd_disconnect ();
	}

	return 0;
}

/* TUNNEL COMMUNICATION HANDLER */
int proto_comm (char *data, unsigned len)
{
#ifndef __WIN32__
	struct proto_ipv6_t *ip = (struct proto_ipv6_t *) data;

	/* we need check incoming destination address
	   because it could be one of our routed client packet */
	if (ipv6_cmp_prefix (tunpr, (unsigned char *) ip->dest))
		return ipv6_send (data, len);
#endif
	/* send received data to virtual TUN device */
	return tundev_send (data, len);
}

/* PROTOCOL PARSER - parse incoming server data */
int proto_parse (char *data, unsigned len)
{
	struct proto_ipv6_t *ip = (struct proto_ipv6_t *) data;
	
	/* check type of packet */
	switch (ip->ver) {
		case PROTO_MAGIC:
			return proto_cmd (data+1, len-1); 
		case PROTO_IPV6:
			return proto_comm (data, len);
	}

	return 0;
}

int proto_init ()
{  

	return 0;
}
