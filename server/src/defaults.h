/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _DEFAULTS_H
#define _DEFAULTS_H

#define DEFAULT_VERSION		"1.1"

#define DEFAULT_ETHDEV		"eth0"
#define DEFAULT_PORT		6060

#define DEFAULT_MTU		1514
#define DEFAULT_BUF_SIZE	DEFAULT_MTU

#define	DEFAULT_HEARTBEAT_MAX	15

#define DEFAULT_CONFIG_PATH	"/etc/tunnel6/server.conf"

#define DEFAULT_DATABASE_PATH	"/etc/tunnel6/db.conf"
#define DEFAULT_DATABASE_UPDATE	5

#define DEFAULT_PIDFILE		"/var/run/t6_server.pid"

#endif
