 _                          _  ____ 
| |                        | |/ ___|
| |_ _   _ _ __  _ __   ___| / /___ 
| __| | | | '_ \| '_ \ / _ \ | ___ \
| |_| |_| | | | | | | |  __/ | \_/ |
 \__|\__,_|_| |_|_| |_|\___|_\_____/

-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

Tunnel6 client is opensource software distributed under GNU/GPL3 license
which provides you connectivity over the IPv6 protocol.

Client uses your IPv4 connectivity to create tunnel between client and server
over the UDP protocol with default port 6060.
It should also go through all NATs and classic firewalls..

It uses TAP-WIN32 device, which is component developed by
the OpenVPN stuff for creating virtual interface and transfering data.
