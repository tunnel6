/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <getopt.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "db.h"
#include "poll.h"
#include "sniff.h"
#include "admin.h"
#include "config.h"
#include "tunnel.h"
#include "packet.h"
#include "signal.h"
#include "daemon.h"
#include "defaults.h"
#include "heartbeat.h"

int cont;

int init ()
{
	if (heartbeat_init () == -1)
		return -1;
  
	if (db_init () == -1)
		return -1;
  
	if (packet_init () == -1)
		return -1;

	if (tunnel_init (config.port) == -1)
		return -1;
	
	if (sniff_init (config.interface, config.ipv6) == -1)
		return -1;

	if (poll_init () == -1)
		return -1;
	
	if (signal_init () == -1)
		return -1;

	cont = 1;
	
	return 0;
}

int loop ()
{
	for (; cont;) {
		if (db_reload () == -1)
			return -1;
	  
		if (poll_loop () == -1)
			return -1;
		
		if (heartbeat_loop () == -1)
			return -1;
	}
	
	return 0;
}

void usage ()
{
	printf ("TUNNEL6 server\nUsage:\n"
		"\t-h : help\n"
		"\t-v : version\n"
		"\t-d : start as daemon\n"
		"\t-f pidfile : specify pid file\n"
		"\t-p port : specify listening port\n"
		"\t-i interface : specify interface\n"
		"\t-l stdout : remap stdout to specified file\n"
		"\t-a ipv6 : specify listening address\n"
		"\t-A name password ipv6 [routedprefix/len] : add DB record\n"
		"\t-U name p;i;r value : update DB record\n"
		"\t-G name : show DB record\n");
}

void parseargs (int argc, char **argv)
{
	int opt;

 	while ((opt = getopt (argc, argv, "hvdp:f:i:l:a:A:U:G:")) > 0) {
		switch (opt) {
			case '?':
			case 'h':
				usage ();
				exit (0);
			case 'v':
				printf ("TUNNEL6 server - version " DEFAULT_VERSION "\n");
				exit (0);
			case 'd':
				config.daemon = 1;
				break;
			case 'p':
				config.port = (unsigned short) atoi (optarg);
				break;
			case 'f':
				config.pidfile = strdup (optarg);
				break;
			case 'i':
				config.interface = strdup (optarg);
				break;
			case 'l':
				stdout = fopen (optarg, "a+");
				
				if (!stdout) {
					perror ("> ERROR -> Log file");
					exit (-1);
				}

				break;
			case 'a':
				config.ipv6 = strdup (optarg);
				break;
			case 'A':
				exit (admin_db_add (argc, argv));
			case 'U':
				exit (admin_db_update (argc, argv));
			case 'G':
				exit (admin_db_get (argc, argv));
		}
	}
}

int main (int argc, char **argv)
{
	/* not an daemon by default */
	config.daemon = 0;
	/* set default pid file */
	config.pidfile = DEFAULT_PIDFILE;
	/* network interface */
	config.interface = DEFAULT_ETHDEV;
	/* listen port */
	config.port = DEFAULT_PORT;
 	/* set automatic ipv6 selection */
	config.ipv6 = 0;
	/* set default backend */
	config.backend = 0;

	config_init ();
	
	parseargs (argc, argv);
	
  	/* unbuffer stdout */
	setvbuf (stdout, (char *) NULL, _IOLBF, 0);

	printf ("TUNNEL6 server v%s by ZeXx86\n", DEFAULT_VERSION);  
	
	if (config.daemon)
		daemonize (config.pidfile);
	
	printf ("Device: %s\n"
		"Port: %u\n", config.interface, config.port);
		
	if (init () == -1)
		return -1;
	
	loop ();
	
	printf ("> tunnel closed\n");
  
	db_disconnect_all ();
	
	return 0;
}
