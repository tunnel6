/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/ioctl.h>
#include <net/ethernet.h>
#include <net/if_arp.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include "db.h"
#include "ipv6.h"
#include "sniff.h"
#include "tunnel.h"
#include "config.h"

extern int errno;
struct netdev_t netdev;

/* receive data from interface */
int sniff_recv ()
{  
	int ret = recv (netdev.fd, netdev.buf, DEFAULT_BUF_SIZE, 0);

	if (ret < 1) {
		perror ("sniff_recv ()");
		return 0;
	}

	netdev.buf[ret] = '\0';

	/* parse received packet */
	return packet_parse (netdev.buf, ret);
}

int sniff_send (char *packet, int packet_size)
{
	/* send the packet */
	if (sendto (netdev.fd_raw, packet, packet_size, 0, (struct sockaddr *) &netdev.s_raw, sizeof (netdev.s_raw)) < 0) {
		perror ("sniff_send ()");
		return -1;
	}
	
	return 0;
}

int sniff_send_ipv6 (char *packet, int packet_size)
{
	struct sockaddr_in6 sockaddr;

	struct proto_ipv6_t *ip = (struct proto_ipv6_t *) packet;

	client_t *c = db_check (ip->dest);
	
	/* this packet should go to another our tunnel client */
	if (c) {
		if (!c->info)
			return 0;

		/* send packet only when our client is online */
		return tunnel_send (c, packet, packet_size);
	}

	/* prepare data for RAW socket */
	memset (&sockaddr, 0x0, sizeof (struct sockaddr_in6));
	sockaddr.sin6_family = AF_INET6;
	sockaddr.sin6_port = 0;
	memcpy (&sockaddr.sin6_addr.s6_addr, ip->dest, sizeof (ipv6_addr_t));

	/* send the packet */
	if (sendto (netdev.fd_raw2, packet, packet_size, 0, (struct sockaddr *) &sockaddr, sizeof (struct sockaddr_in6)) != packet_size) {
		perror ("sniff_send_ipv6 ()");
		return -1;
	}
	
	return 0;
}

/* initialize a packet sniffer */
int sniff_init (char *dev, char *listenipv6)
{
	struct packet_mreq req;
	struct ifreq ifr;
	
	/** create socket for sniffing  */
	if ((netdev.fd = socket (AF_PACKET, SOCK_RAW, htons (ETH_P_ALL))) == -1) {
		printf ("ERROR -> socket ()\n");
		return -1;
	}

	/* set the interface */
	strncpy (ifr.ifr_name, dev, IFNAMSIZ);

	/* get the interface structure */
	if (ioctl (netdev.fd, SIOCGIFINDEX, &ifr) == -1) {
		printf ("ERROR -> invalid interface %s\n", dev);
		return -1;
	}
	
	/* set the promiscuous mode on 'dev' interface */
	memset (&req, 0, sizeof (struct packet_mreq));
	
	req.mr_ifindex = ifr.ifr_ifindex;
	req.mr_type = PACKET_MR_PROMISC;
	
	netdev.ifindex = ifr.ifr_ifindex;
	
 	/* get mac address */
	if (ioctl (netdev.fd, SIOCGIFHWADDR, &ifr) == -1) {
		printf ("ERROR -> Can't get MAC address from interface %s\n", dev);
		return -1;
	}
	
	/* remember mac address of the ethernet */
	memcpy (netdev.hwaddr, ifr.ifr_hwaddr.sa_data, sizeof (mac_addr_t));
	
	 /* get IPv6 address */	 
	struct ifaddrs *myaddrs, *ifa;
	struct sockaddr_in6 *s6;

	/* get interface configuration */
	if (getifaddrs (&myaddrs)) {
		printf ("ERROR -> Can't get address list from OS\n");
		return -1;
	}
	
	struct sockaddr_in6 ip;
	
	if (listenipv6)
		inet_pton (AF_INET6, listenipv6, &ip);

	for (ifa = myaddrs; ifa; ifa = ifa->ifa_next) {
		if (!ifa->ifa_addr)
			continue;
		if (strcmp (ifa->ifa_name, dev))
			continue;
		if (!(ifa->ifa_flags & IFF_UP))
			continue;
		if (!(ifa->ifa_flags & IFF_RUNNING))
			continue;
		if (ifa->ifa_addr->sa_family != AF_INET6)
			continue;
		
		s6 = (struct sockaddr_in6 *) ifa->ifa_addr;
		
		unsigned short *a = (unsigned short *) &s6->sin6_addr;
		/* we dont need an link local address */
		if (*a == 0x80fe)
			continue;
		
		/* check for user-specified IPv6 */
		if (listenipv6) {
			if (ipv6_cmp ((unsigned short *) &ip, a)) {
				memcpy (netdev.ip, &s6->sin6_addr, sizeof (ipv6_addr_t));
				break;
			}
		} else
			memcpy (netdev.ip, &s6->sin6_addr, sizeof (ipv6_addr_t));

		break;
	}

	if (!netdev.ip[0]) {
		printf ("ERROR -> invalid listening IPv6 [%s]\n", ipv6_print (netdev.ip));
		return -1;
	}

	printf ("Address: %s\n", ipv6_print (netdev.ip));

	if (setsockopt (netdev.fd, SOL_PACKET, PACKET_ADD_MEMBERSHIP, (char *) &req, sizeof(req)) == -1) {
		printf ("ERROR -> setsockopt ()\n");
		return -1;
	}
	
	/** RAW sender init */

	/* initialize raw socket */
	if ((netdev.fd_raw = socket (AF_PACKET, SOCK_RAW, htons (ETH_P_ALL))) == -1) {
		fprintf (stderr, "Couldn't open RAW socket.\n");
		perror ("socket()");
		return -1;
	}
	
	/* prepare data for RAW socket */
	netdev.s_raw.sll_family	= PF_PACKET;		/* RAW communication */
	netdev.s_raw.sll_protocol = htons (ETH_P_IP);	/* protocol above the ethernet layer */
	netdev.s_raw.sll_ifindex = netdev.ifindex;	/* set index of the network device */
	netdev.s_raw.sll_hatype = ARPHRD_ETHER;
	netdev.s_raw.sll_pkttype = PACKET_OTHERHOST;	/* target host is another host */
	netdev.s_raw.sll_halen = 0;
	/*MAC - begin*/
	memcpy (&netdev.s_raw.sll_addr, netdev.hwaddr, 6);
	/*MAC - end*/
	netdev.s_raw.sll_addr[6]  = 0x00;
	netdev.s_raw.sll_addr[7]  = 0x00;

	/* set the interface */
	strncpy (ifr.ifr_name, dev, IFNAMSIZ);
	
	/* bind the socket to the interface */
	if (setsockopt (netdev.fd_raw, SOL_SOCKET, SO_BINDTODEVICE, &ifr, sizeof (struct ifreq)) == -1){
		fprintf (stderr, "Couldn't bind the socket to the interface.\n");
		perror ("setsockopt()");
		return -1;
	}
	
	/** IPv6 RAW sender init */
	
	/* initialize raw socket */
	if ((netdev.fd_raw2 = socket (AF_INET6, SOCK_RAW, IPPROTO_RAW)) == -1) {
		fprintf (stderr, "Couldn't open RAW socket.\n");
		perror ("socket()");
		return -1;
	}
	
	return 0;
}
