/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __WIN32__
# include <sys/time.h>
# include <stdio.h>
#else
# include <windows.h>
#endif
#include "proto.h"
#include "tunnel.h"
#include "defaults.h"

#ifndef __WIN32__
static struct timeval tnow, tnext;
#else
long long tnow, tnext;
#endif
static char heartbeat_msg[2];

/* Send periodicaly heartbeat packet to tunnel server */
int heartbeat_send ()
{
	return tunnel_send (heartbeat_msg, 2);
}

int heartbeat_loop ()
{
#ifndef __WIN32__
	/* 1s periodic cache flush */
	gettimeofday (&tnow, NULL);
	
	if (timercmp (&tnow, &tnext, >=)) {
		tnext = tnow;
		tnext.tv_sec += DEFAULT_HEARTBEAT;
		
		return heartbeat_send ();
	}
#else
	tnow = GetTickCount () / 1000;

	if ((tnow - tnext) >= DEFAULT_HEARTBEAT) {
		tnext = tnow;
		return heartbeat_send ();
	}
#endif
	return 0;
}

int heartbeat_init ()
{
	/* build */
	heartbeat_msg[0] = PROTO_MAGIC;
  	heartbeat_msg[1] = PROTO_CMD_HEARTBEAT;
	
	/* get current time */
#ifndef __WIN32__
	gettimeofday (&tnext, NULL);
	tnext.tv_sec += DEFAULT_HEARTBEAT;
#else
	tnext = GetTickCount () / 1000;
#endif
	return 0;
}
