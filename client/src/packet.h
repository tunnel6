/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _PACKET_H
#define _PACKET_H

#define PACKET_TYPE_IPV6	0xdd86

/* MAC address */
typedef unsigned char mac_addr_t[6];

/* ethernet header structure */
struct proto_eth_t {
	mac_addr_t dest;	/* destination MAC addr */
	mac_addr_t src;		/* source MAC addr */
	unsigned short type;	/* packet type */
} __attribute__ ((__packed__));

#endif
