/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _CONFIG_H
#define _CONFIG_H

#define CONFIG_VAR_INTERFACE	"interface"
#define CONFIG_VAR_IPV6		"ipv6"
#define CONFIG_VAR_PORT		"port"
#define CONFIG_VAR_BACKEND	"backend"

#define CONFIG_VAR_MYSQL_HOST	"mysql_host"
#define CONFIG_VAR_MYSQL_USER	"mysql_user"
#define CONFIG_VAR_MYSQL_PWD	"mysql_pwd"
#define CONFIG_VAR_MYSQL_DB	"mysql_db"

typedef struct {
	char *interface;
	char *ipv6;
	unsigned short port;
	char *backend;
	char daemon;
	char *pidfile;
	
	/* mysql backend */
	char *mysql_host;
	char *mysql_user;
	char *mysql_pwd;
	char *mysql_db;
} config_t;

extern config_t config;
extern int config_init ();

#endif
