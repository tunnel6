/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include "db.h"
#include "defaults.h"

struct timeval tnow;
extern client_t client_list;

int heartbeat_update (client_t *c)
{
	c->tnow = tnow;
	c->tnow.tv_sec += DEFAULT_HEARTBEAT_MAX;

	return 0;
}

int heartbeat_loop ()
{
	/* 1s periodic cache flush */
	gettimeofday (&tnow, NULL);
	
	client_t *client;
	for (client = client_list.next; client != &client_list; client = client->next) {
		/* is client online ? */
		if (!client->info)
			continue;
		
		/* check when timer exceed max timeout */
		if (timercmp (&tnow, &client->tnow, >=)) {
			printf ("WARNING -> client '%s' timeout !\n", client->name);
			
			return db_flush (client);
		}
	}
  
	return 0;
}

int heartbeat_init ()
{
	/* 1s periodic cache flush */
	gettimeofday (&tnow, NULL);
	
	return 0;
}
