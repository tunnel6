/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __WIN32__
#ifndef __linux__
#ifndef __APPLE_CC__
# include <net/if_tun.h>
#endif
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <arpa/inet.h> 
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "ipv6.h"
#include "tunnel.h"
#include "packet.h"
#include "defaults.h"

static int fd;					/* FD of the TUN/TAP device */
static char data[DEFAULT_MTU+1];
static char dev[IFNAMSIZ+1];

extern int fd_tun;				/* polling FD of the TUN/TAP device */

extern ipv6_addr_t tunip;			/* IPv6 address obtained from tunnel server */
extern ipv6_addr_t tungw;			/* IPv6 gateway obtained from tunnel server */
extern ipv6_prefix_t tunpr;			/* IPv6 prefix obtained from tunnel server */

static int tundev_arch_alloc ()
{
	/* try to load tun module / we dont know when its module or built-in */
	int r = system ("ifconfig tun0 create 2>/dev/null >/dev/null");
  
	r = 0;

	char *clonedev = "/dev/tun0";

	if( (fd = open (clonedev, O_RDWR)) < 0 ) {
		perror ("Opening " DEFAULT_TUNTAP_DEV);
		return fd;
	}

	strcpy (dev, "tun0");

	return fd;
}

int tundev_arch_recv ()
{  
	int ret = read (fd, data, DEFAULT_MTU);

	if (ret < 1) {
		return 0;
	}
	
	data[ret] = '\0';
	
	return ret;
}

int tundev_arch_poll ()
{
	int ret = tundev_arch_recv ();
	
	if (!ret)
		return 0;
	
	struct proto_ipv6_t *ip = (struct proto_ipv6_t *) &data;

	if (!ipv6_cmp (ip->src, tunip) && !ipv6_cmp_prefix (tunpr, (unsigned char *) ip->src)) {
		printf ("WARNING -> This is not our packet\n");
		return 0;
	}

	return tunnel_send (data, ret);
}

int tundev_arch_send (char *packet, unsigned len)
{
        if (write (fd, packet, len) == -1) {
		printf ("ERROR -> tap_send ()\n");
		return -1;
	}
	
	return 0;
}

int tundev_arch_setup ()
{
	sprintf (data, "ifconfig %s up", dev);
	int r = system (data);

	char ipv6[64];
	inet_ntop (AF_INET6, &tunip, ipv6, 64);
	
	sprintf (data, "ifconfig %s inet6 %s/128", dev, ipv6);
	r = system (data);
	
	char ipv6_[64];
	inet_ntop (AF_INET6, &tungw, ipv6_, 64);

	sprintf (data, "route add -inet6 %s -prefixlen 128 %s", ipv6_, ipv6);
	r = system (data);
	
	sprintf (data, "route add -inet6 default %s", ipv6_);
	r = system (data);
	
	return 0;
}

int tundev_arch_init ()
{
	/* initialize TUN interface, which operates on 3rd network layer (IP) */
	fd_tun = tundev_arch_alloc ();

	if (fd_tun < 0) {
		printf ("ERROR -> TUN/TAP interface '%s' failed\n", DEFAULT_TUNNEL6_DEV);
		return -1;
	}
	
	printf ("> Tunnel device '%s' was created\n", dev);
  
	return 0;
}
#endif
#endif
