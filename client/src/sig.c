/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include "defaults.h"

extern int cont;

void sig_term (int z)
{
	printf ("> WARNING -> term signal -> type %d\n", z);
	
	cont = 0;
}

int sig_init ()
{
#ifndef __WIN32__
	struct sigaction sv;  
	
	memset (&sv, 0, sizeof (struct sigaction));
	sv.sa_flags = 0;
	sigemptyset (&sv.sa_mask);

	sv.sa_handler = SIG_IGN;
	
	/* Don't want broken pipes to kill the client.  */
	sigaction (SIGPIPE, &sv, NULL);
	
	/* ...or any defunct child processes.  */
	sigaction (SIGCHLD, &sv, NULL);
	
	sv.sa_handler = sig_term;
	
	/* Also, shut down properly.  */
	sigaction (SIGTERM, &sv, NULL);
	sigaction (SIGINT, &sv, NULL);
#endif
	return 0;
}
