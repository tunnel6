/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef __WIN32__

#include <windows.h>
#include <objbase.h>
#include <winioctl.h>
#include <ws2tcpip.h>
#include <stdio.h>
#include "ipv6.h"
#include "tunnel.h"
#include "packet.h"
#include "defaults.h"

/*
 * Thanks for OpenVPN to TAP-WIN32
 * Thanks for CloudVPN, respectively P2PVPN
 * Without them this code could'nt exist
 */

/* Registry IDs of TAP device */
#define TAP_ADAPTER_KEY				"SYSTEM\\CurrentControlSet\\Control\\Class\\{4D36E972-E325-11CE-BFC1-08002BE10318}"
#define NETWORK_CONNECTIONS_KEY			"SYSTEM\\CurrentControlSet\\Control\\Network\\{4D36E972-E325-11CE-BFC1-08002BE10318}"

/* Driver name of the TUN/TAP required by tunnel6 */
#define TAP_COMPONENT_ID 			"tap0901"

#define USERMODEDEVICEDIR 			"\\\\.\\Global\\"
#define TAPSUFFIX         			".tap"

#define TAP_CONTROL_CODE(request,method)	CTL_CODE (FILE_DEVICE_UNKNOWN, request, method, FILE_ANY_ACCESS)

#define TAP_IOCTL_SET_MEDIA_STATUS      	TAP_CONTROL_CODE (6, METHOD_BUFFERED)

/* NDP Neighbour packet type */
#define NDP_TYPE_NBSOL				135
#define NDP_TYPE_NBADV				136

/* NDP protool header */
struct proto_ndp_t {
	unsigned char type;
	unsigned char code;
	unsigned short checksum;
	unsigned flags;
	ipv6_addr_t target;
	
	/* ICMPv6 options */
	unsigned char ll_type;
	unsigned char ll_length;
	mac_addr_t ll_mac;
};

/* pseudo-header for icmp6 checksum */
typedef struct proto_ipv6_pseudo_t {
	unsigned short ip_source[8];
	unsigned short ip_dest[8];
	unsigned pl_len;
	unsigned zero:24;
	unsigned char next_head;
} proto_ipv6_pseudo_t;

static HANDLE fd;				/* FD of TUN/TAP device */
static OVERLAPPED o_read, o_write;

static int read_in_progress = 0;
static DWORD read_result = 0;

/* data buffers */
static char buf_prealloc[DEFAULT_MTU + 64];
static char data[DEFAULT_MTU + 64];
static char data_thr[DEFAULT_MTU + 64];

static char *dev;				/* System name of the device */

int tundev_arch_send (char *packet, unsigned len);

extern ipv6_addr_t tunip;
extern ipv6_addr_t tungw;

static int findTapDevice (char *deviceID, int deviceIDLen, char *deviceName, int deviceNameLen)
{
	HKEY adapterKey;
	int i;
	LONG status;
	DWORD len;
	char keyI[1024];
	char keyName[1024];
	HKEY key;

	status = RegOpenKeyEx (HKEY_LOCAL_MACHINE, TAP_ADAPTER_KEY, 0, KEY_READ, &adapterKey);

	if (status != ERROR_SUCCESS) {
		printf ("Could not open key '%s'!\n", TAP_ADAPTER_KEY);
		return 1;
	}

	strncpy (deviceID, "", deviceIDLen);

	for (i = 0; !deviceID[0] && RegEnumKey (adapterKey, i, keyI, sizeof (keyI)) == ERROR_SUCCESS; i++) {
		char componentId[256];

		snprintf (keyName, sizeof (keyName), "%s\\%s", TAP_ADAPTER_KEY, keyI);

		status = RegOpenKeyEx (HKEY_LOCAL_MACHINE, keyName, 0, KEY_READ, &key);

		if (status != ERROR_SUCCESS) {
			printf ("Could not open key '%s'!\n", keyName);
			return 1;
		}

		len = sizeof (componentId);
		status = RegQueryValueEx (key, "ComponentId", NULL, NULL, (BYTE *) componentId, &len);

		if (status == ERROR_SUCCESS && !strcmp (componentId, TAP_COMPONENT_ID)) {
			len = deviceIDLen;
			RegQueryValueEx (key, "NetCfgInstanceId", NULL, NULL, (BYTE *) deviceID, &len);
		}

		RegCloseKey (key);
	}

	RegCloseKey (adapterKey);

	if (deviceID[0] == 0)
		return 1;

	snprintf (keyName, sizeof (keyName), "%s\\%s\\Connection", NETWORK_CONNECTIONS_KEY, deviceID);
	
	status = RegOpenKeyEx (HKEY_LOCAL_MACHINE, keyName, 0, KEY_READ, &key);
	
	if (status != ERROR_SUCCESS)
		return 1;

	len = deviceNameLen;
	status = RegQueryValueEx (key, "Name", NULL, NULL, (BYTE *) deviceName, &len);
	
	RegCloseKey (key);

	if (status != ERROR_SUCCESS)
		return 1;

	return 0;
}

static int tundev_arch_alloc ()
{
	char deviceId[256];
	char deviceName[256];
	char tapPath[256];
	unsigned long len = 0;
	int status;

	if (findTapDevice (deviceId, sizeof (deviceId), deviceName, sizeof (deviceName))) {
		printf ("ERROR -> Can't find TAP device !\n");
		return -1;
	}

	dev = strdup (deviceName);
	
	if (!dev)
		return -1;

	printf ("Device: '%s'\n", deviceName);

	snprintf (tapPath, sizeof (tapPath), "%s%s%s", USERMODEDEVICEDIR, deviceId, TAPSUFFIX);

	fd = CreateFile (tapPath, GENERIC_READ | GENERIC_WRITE, 0, 0,
			 OPEN_EXISTING, FILE_ATTRIBUTE_SYSTEM | FILE_FLAG_OVERLAPPED, 0);

	if (fd == INVALID_HANDLE_VALUE) {
		printf ("Could not open `%s'!\n", tapPath);
		return -1;
	}

	status = TRUE;

	/* set device status to UP */
	DeviceIoControl (fd, TAP_IOCTL_SET_MEDIA_STATUS, &status, sizeof (status),
	                 &status, sizeof (status), &len, NULL);

	o_read.Offset = 0;
	o_read.OffsetHigh = 0;
	o_write.Offset = 0;
	o_write.OffsetHigh = 0;

	printf ("> TAP -> OK\n");

	return 0;
}

/* receive packet from TAP device */
int tundev_arch_recv ()
{
	if (read_in_progress) {
		if (!GetOverlappedResult (fd, &o_read, &read_result, FALSE) )
			return -1;

		read_in_progress = 0;

		return read_result;
	}

	if (ReadFile (fd, data_thr, DEFAULT_MTU, &read_result, &o_read))
		return read_result;

	int i = GetLastError ();

	if (i == ERROR_IO_PENDING)
		read_in_progress = 1;
	else
		printf ("error %d on iface read\n", i);

	return -1;
}

unsigned short checksum16_ones (unsigned sum, const void *_buf, int len)
{
	const unsigned short *buf = _buf;

	while (len >= 2) {
		sum += *buf ++;

		if (sum & 0x80000000)
			sum = (sum & 0xffff) + (sum >> 16);

		len -= 2;
	}

	if (len) {
		unsigned char temp[2];

		temp[0] = *(unsigned char *) buf;
		temp[1] = 0;

		sum += *(unsigned short *) temp;
	}

	while (sum >> 16)
		sum = (sum & 0xffff) + (sum >> 16);

	return sum;
}

unsigned short checksum16 (void *_buf, int len)
{
	return ~checksum16_ones (0, _buf, len);
}

/* calculate packet checksum */
unsigned short checksum16_ipv6 (unsigned short ip_source[8], unsigned short ip_dest[8], char *buf, unsigned len, unsigned char proto)
{
	char *buf_pseudo = (char *) &buf_prealloc;

	if (!buf_pseudo)
		return 0;

	proto_ipv6_pseudo_t *pseudo = (proto_ipv6_pseudo_t *) buf_pseudo;

	memcpy (pseudo->ip_source, ip_source, 16);
	memcpy (pseudo->ip_dest, ip_dest, 16);

	pseudo->pl_len = htonl (len);
	pseudo->zero = 0x0;
	pseudo->next_head = proto;

	memcpy (buf_pseudo+40, buf, len);

	buf_pseudo[len+40] = '\0';

	unsigned short ret = checksum16 (buf_pseudo, len+40);

	return ret;
}

/* send neighbour advertisement - we lie about existing address  :-B */
int ndp_reply (char *buf)
{
	struct proto_ipv6_t *ip = (struct proto_ipv6_t *) buf;
	struct proto_ndp_t *ndp = (struct proto_ndp_t *) ((char *) buf + sizeof (struct proto_ipv6_t));

	/* check for neighbour solicitation packet */
	if (ndp->type != NDP_TYPE_NBSOL)
		return -1;
	
	/* build a new packet */
	char packet[sizeof (struct proto_ipv6_t) + sizeof (struct proto_ndp_t) + 1];
	
	/* IPv6 layer */
	struct proto_ipv6_t *packet_ip = (struct proto_ipv6_t *) ((char *) &packet);
	
	packet_ip->ver = 0x60;
	packet_ip->tclass = 0x0;
	packet_ip->flabel = 0x0;
	packet_ip->pl_len = htons (32);
	packet_ip->nhead = IPV6_TYPE_ICMPv6;
	packet_ip->hop = 0xff;
	memcpy (packet_ip->src, (void *) ndp->target, sizeof (ipv6_addr_t));
	memcpy (packet_ip->dest, (void *) ip->src, sizeof (ipv6_addr_t));
	
	/* ICMPv6 - NDP layer */
	struct proto_ndp_t *packet_ndp = (struct proto_ndp_t *) ((char *) &packet + sizeof (struct proto_ipv6_t));

	packet_ndp->type = NDP_TYPE_NBADV;
	packet_ndp->code = 0;
	packet_ndp->flags = htonl (0xe0);
	memcpy (packet_ndp->target, (void *) ndp->target, sizeof (ipv6_addr_t));

	packet_ndp->ll_type = 2;
	packet_ndp->ll_length = 1;
	
	/* fake MAC address */
	packet_ndp->ll_mac[0] = 0x00;
	packet_ndp->ll_mac[1] = 0xff;
	packet_ndp->ll_mac[2] = 0x25;
	packet_ndp->ll_mac[3] = 0x02;
	packet_ndp->ll_mac[4] = 0x19;
	packet_ndp->ll_mac[5] = 0x78;

	packet_ndp->checksum = 0x0;
	
	/* calculate checksum */
	packet_ndp->checksum = checksum16_ipv6 (packet_ip->src, packet_ip->dest, (char *) packet_ndp, sizeof (struct proto_ndp_t), IPV6_TYPE_ICMPv6);
	
	/* send RAW packet */
	return tundev_arch_send (packet, sizeof (struct proto_ipv6_t) + sizeof (struct proto_ndp_t));
}

DWORD WINAPI tundev_recv_thread (LPVOID arg)
{
	for (;;) {
		int ret = tundev_arch_recv ();
		
		if (ret < 1) {
			Sleep (1);
			continue;
		}

		struct proto_ipv6_t *ip = (struct proto_ipv6_t *) ((char *) &data_thr + sizeof (struct proto_eth_t));
		
		if (!ipv6_cmp (ip->src, tunip)) {
		//	printf ("WARNING -> This is not our packet\n");
			continue;
		}
		
		if (!ndp_reply ((char *) ip))
			continue;

		tunnel_send ((char *) ip, ret - sizeof (struct proto_eth_t));
	}
	
	return 0;
}

int tundev_arch_poll ()
{
	/* replaced by thread */
	return 0;
}

int tundev_arch_send (char *packet, unsigned len)
{
	struct proto_eth_t *eth = (struct proto_eth_t *) &data;
  
	/* build IPv6 multicast packet */
	eth->dest[0] = 0x33;
	eth->dest[1] = 0x33;
	eth->dest[2] = 0xff;
	eth->dest[3] = 0x00;
	eth->dest[4] = 0x00;
	eth->dest[5] = 0x02;
	
	eth->src[0] = 0x00;
	eth->src[1] = 0xff;
	eth->src[2] = 0x53;
	eth->src[3] = 0x52;
	eth->src[4] = 0x27;
	eth->src[5] = 0x68;

	eth->type = PACKET_TYPE_IPV6;
  
	memcpy (data+sizeof (struct proto_eth_t), packet, len);
  
	if (!WriteFile (fd, data, sizeof (struct proto_eth_t) + len, 0, &o_write)) {
		/*we need to wait for operation to complete,
		  because if there was any overlapping, memory corruption
		  would happen. */
		DWORD len = 0;

		if (!GetOverlappedResult (fd, &o_write, &len, TRUE))
			printf ("ERROR -> writing iface failed with %d", (int) GetLastError ());
	}

	return 0;
}

#ifndef inet_ntop
const char *inet_ntop (int af, const void *src, char *dst, socklen_t cnt)
{
	if (af == AF_INET) {
		struct sockaddr_in in;
		memset (&in, 0, sizeof (in));
		in.sin_family = AF_INET;
		memcpy (&in.sin_addr, src, sizeof (struct in_addr));
		getnameinfo ((struct sockaddr *) &in, sizeof (struct
			      sockaddr_in), dst, cnt, NULL, 0, NI_NUMERICHOST);

		return dst;
	} else if (af == AF_INET6) {
		struct sockaddr_in6 in;
		memset(&in, 0, sizeof (in));
		in.sin6_family = AF_INET6;
		memcpy (&in.sin6_addr, src, sizeof (struct in_addr6));
		getnameinfo ((struct sockaddr *) &in, sizeof (struct
			      sockaddr_in6), dst, cnt, NULL, 0, NI_NUMERICHOST);

		return dst;
	}

	return 0;
}
#endif

int tundev_arch_setup ()
{
	/* needed for XP */
	int r = system ("ipv6 install > NUL");
	
	char ipv6[64];
	inet_ntop (AF_INET6, &tunip, ipv6, 64);
	
	sprintf (data, "netsh interface ipv6 add address \"%s\" %s > NUL", dev, ipv6);
	printf ("> Setting new interface address: ");
	r = system (data);
	
	if (!r)
		printf ("OK\n");
	
	inet_ntop (AF_INET6, &tungw, ipv6, 64);

	sprintf (data, "netsh interface ipv6 add route ::/0 \"%s\" %s > NUL", dev, ipv6);
	printf ("> Setting new interface gateway: ");
	r = system (data);

	if (!r)
		printf ("OK\n");
	
	return 0;
}

int tundev_arch_init ()
{
	if (tundev_arch_alloc () == -1)
		return -1;
	
	DWORD pID;
	HANDLE h = CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE) tundev_recv_thread, 0, 0, &pID); 
  
	if (!h) {
		printf ("ERROR -> receiver thread failed\n");
		return -1;
	}
		
	return 0;
}
#endif
