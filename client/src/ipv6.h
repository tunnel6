/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _IPV6_H
#define _IPV6_H

#define IPV6_TYPE_ICMPv6		0x3a

/* IPv6 address */
typedef unsigned short ipv6_addr_t[8];	/* used to store ipv6 address */
typedef unsigned char ipv6_prefix_t[16];/* used to store the prefix address - last byte is length */

/* IPv6 header structure */
struct proto_ipv6_t {
	unsigned char ver;		/* version */
	unsigned char tclass;		/* traffic class */
	unsigned short flabel;		/* flow label */

	unsigned short pl_len;		/* payload length*/
  
	unsigned char nhead;		/* next header */

	unsigned char hop;		/* hop limit */

	ipv6_addr_t src;		/* source ip address */
	ipv6_addr_t dest;		/* destination ip address */	
};

extern unsigned ipv6_cmp (ipv6_addr_t ip, ipv6_addr_t ip2);
extern unsigned ipv6_cmp_prefix (ipv6_prefix_t pr, ipv6_prefix_t ip);
extern char *ipv6_print (ipv6_addr_t ip);
extern int ipv6_send (char *data, int len);
extern int ipv6_init ();

#endif
