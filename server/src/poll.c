/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <sys/socket.h>
#include <stdio.h>
#include "sniff.h"
#include "tunnel.h"

extern struct netdev_t netdev;
static struct timeval tv;
static fd_set set;
static int max;

int poll_loop ()
{
	/* clear set */
	FD_ZERO (&set);
	/* add fd to set */
	FD_SET (netdev.fd, &set);
	FD_SET (netdev.fd_tun, &set);
  
	/* set time period */
	tv.tv_sec = 0;
	tv.tv_usec = 10000;
	
	/* polling system - it would block whole program, but save cpu load */
	int s = select (max + 1, &set, NULL, NULL, &tv);
	
	if (s < 1)
		return 0;

	if (FD_ISSET (netdev.fd, &set))
		sniff_recv ();

	if (FD_ISSET (netdev.fd_tun, &set))
		tunnel_recv ();

	return 0;
}

int poll_init ()
{
	/* set the highest ID of FD */
	max = netdev.fd_tun;
	
	if (max < netdev.fd)
		max = netdev.fd;
  
	return 0;
}
