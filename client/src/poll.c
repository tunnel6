/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef  __WIN32__
# include <sys/socket.h>
# include <sys/select.h>
#else
# include <windows.h>
#endif
#include <stdio.h>
#include "tunnel.h"
#include "tundev.h"

static struct timeval tv;
static fd_set set;
static int max;

#ifndef  __WIN32__
int fd_udp;		/* polling FD of the tunnel connection */
int fd_tun;		/* polling FD of the TUN/TAP device */
#else
SOCKET fd_udp;		/* polling FD of the tunnel connection */
#endif

int poll_loop ()
{
	/* clear set */
	FD_ZERO (&set);
	/* add fd to set */
	FD_SET (fd_udp, &set);
#ifndef  __WIN32__
	FD_SET (fd_tun, &set);
#endif
	/* set time period */
	tv.tv_sec = 0;
	tv.tv_usec = 10000;

	/* polling system - it would block whole program, but save cpu load */
	int s = select (max + 1, &set, NULL, NULL, &tv);

	if (s < 1)
		return 0;

	if (FD_ISSET (fd_udp, &set))
		tunnel_poll ();
#ifndef  __WIN32__
	if (FD_ISSET (fd_tun, &set))
		tundev_poll ();
#endif
	return 0;
}

int poll_init ()
{
	/* set the highest ID of FD */
	max = fd_udp;
#ifndef  __WIN32__
	if (max < fd_tun)
		max = fd_tun;
#endif  
	return 0;
}
