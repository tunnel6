/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <netdb.h>
#include "db.h"
#include "proto.h"
#include "sniff.h"
#include "packet.h"
#include "defaults.h"

extern struct netdev_t netdev;
static struct sockaddr_in sockname;
static struct sockaddr_in clientinfo;
static socklen_t addrlen;
static char data[DEFAULT_MTU+1];

int tunnel_recv ()
{  
	int ret = recvfrom (netdev.fd_tun, data, DEFAULT_MTU, 0, (struct sockaddr *) &clientinfo, &addrlen);

	if (ret < 1) {
		//printf ("ERROR -> recvfrom ()\n");
		return 0;
	}

	data[ret] = '\0';
	
	return proto_parse (&clientinfo, data, ret);
}

int tunnel_send (client_t *c, char *data, unsigned len)
{
        if (sendto (netdev.fd_tun, data, len, 0, (struct sockaddr *) c->info, addrlen) == -1) {
		printf ("ERROR -> tunnel_send () -> sendto ()\n");
		return -1;
	}
	
	return 0;
}


int tunnel_init (unsigned short port)
{  
	if ((netdev.fd_tun = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
		printf ("ERROR -> socket ()\n");
		return -1;
	}

	sockname.sin_family = AF_INET;
	sockname.sin_port = htons (port);
	sockname.sin_addr.s_addr = INADDR_ANY;

	if (bind (netdev.fd_tun, (struct sockaddr *) &sockname, sizeof (sockname)) == -1) {
		printf ("ERROR -> bind ()\n");
		return -1;
	}
		
	addrlen = sizeof (clientinfo);

	return 0;
}
