/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "db.h"
#include "ipv6.h"
#include "sniff.h"
#include "proto.h"
#include "tunnel.h"
#include "packet.h"
#include "defaults.h"
#include "heartbeat.h"

extern struct netdev_t netdev;

/* PROTOCOL Command - LOGIN */
int proto_cmd_login (struct sockaddr_in *info, char *data, unsigned len)
{
	/* obtain name and password from packet */
	unsigned char name_len = data[0];
	unsigned char pwd_len = data[1];
	
	char *name = strndup (data+2, name_len);
	
	if (!name)
		return -1;
	
	char *pwd = strndup (data+2+name_len, pwd_len);
	
	if (!pwd)
		return -1;

	/* verify name and password with database */
	client_t *c = db_verify (name, pwd);
	
	if (!c) {
		printf ("Invalid login - username: '%s'\n", name);
		free (pwd);
		free (name);
		
		/* build the answer - multiple login */
		char answer = PROTO_CMD_LOGIN_FAIL;

		client_t f;
		f.info = info;
		/* send packet over IPv4 tunnel connection to client */
		return tunnel_send (&f, &answer, 1);
	}
	
	if (!c->info)
		printf ("> Login succesfull: %s\n", name);
	else {
		printf ("WARNING -> multiple login, user: %s\n", name);
		free (pwd);
		free (name);
		
		/* build the answer - multiple login */
		char answer = PROTO_CMD_LOGIN_MULTI;

		client_t f;
		f.info = info;
		/* send packet over IPv4 tunnel connection to client */
		return tunnel_send (&f, &answer, 1);
	}

	free (pwd);
	free (name);
	
	heartbeat_update (c);
	
	c->info = (struct sockaddr_in *) malloc (sizeof (struct sockaddr_in));
	
	if (!c->info) {
		printf ("ERROR -> !c->info\n");
		return -1;
	}
	
	/* save client data (IPv4 address, UDP Port) */
	memcpy (c->info, info, sizeof (struct sockaddr_in));
	
	/* build the answer - login succefull + ipv6 addr from db + gw */
	char answer[2 + 3*sizeof (ipv6_addr_t)];
	
	answer[0] = PROTO_CMD_LOGIN_OK;							/* protocol command */
	memcpy (answer+1, c->ip, sizeof (ipv6_addr_t));					/* client ipv6 */
	memcpy (answer+1+sizeof (ipv6_addr_t), netdev.ip, sizeof (ipv6_addr_t));	/* gateway for client */
	memcpy (answer+1+2*sizeof (ipv6_addr_t), c->prefix, sizeof (ipv6_addr_t));	/* routed prefix of client */
	
	/* send packet over IPv4 tunnel connection to client */
	return tunnel_send (c, answer, 1 + 3*sizeof (ipv6_addr_t));
}

/* PROTOCOL Command - HEARTBEAT */
int proto_cmd_heartbeat (struct sockaddr_in *info)
{
	client_t *c = db_find (info);
	
	if (!c) {
		char ip[16];
		if (!inet_ntop (AF_INET, (void *) &info->sin_addr, ip, 15))
			return 0;
	  
		printf ("WARNING -> heartbeat from unknown client (%s)\n", ip);
		
		/* build command */
		char req[2];
		req[0] = PROTO_MAGIC;
		req[1] = PROTO_CMD_DISCONNECT;
		
		client_t f;
		f.info = info;

		/* disconnect client which lost connection */
		return tunnel_send (&f, req, 2);
	}

	return heartbeat_update (c);
}

/* PROTOCOL Command - DISCONNECT */
int proto_cmd_disconnect (struct sockaddr_in *info)
{
	client_t *c = db_find (info);

	if (!c) {
		printf ("WARNING -> unknown client disconnected\n");
		return 0;
	}
	
	printf ("> client '%s' disconnected\n", c->name);

	return db_flush (c);
}

/* PROTOCOL Command - DISCONNECT by server */
int proto_cmd_disconnect2 (client_t *c)
{
	printf ("> client '%s' disconnected by server\n", c->name);

	/* build command */
	char req[2];
	req[0] = PROTO_MAGIC;
	req[1] = PROTO_CMD_DISCONNECT;
	
	tunnel_send (c, req, 2);
	
	return db_flush (c);
}

/* TUNNEL PROTOCOL HANDLER */
int proto_cmd (struct sockaddr_in *info, char *data, unsigned len)
{
	char *proto = (char *) data;
	
	switch (*proto) {
		case PROTO_CMD_LOGIN:
			return proto_cmd_login (info, data+1, len-1);
		case PROTO_CMD_HEARTBEAT:
			return proto_cmd_heartbeat (info);
		case PROTO_CMD_DISCONNECT:
			return proto_cmd_disconnect (info);
	}

	return 0;
}

/* TUNNEL COMMUNICATION HANDLER */
int proto_comm (struct sockaddr_in *info, char *data, unsigned len)
{
	/* send client packet over native IPv6 to its destination */
	return sniff_send_ipv6 (data, len);
}

/* PROTOCOL PARSER - parse incoming client data */
int proto_parse (struct sockaddr_in *info, char *data, unsigned len)
{
	struct proto_ipv6_t *ip = (struct proto_ipv6_t *) data;
	
	/* check type of packet */
	switch (ip->ver) {
		case PROTO_MAGIC:
			return proto_cmd (info, data+1, len-1); 
		case PROTO_IPV6:
			return proto_comm (info, data, len);
	}

	return 0;
}

int proto_init ()
{  

	return 0;
}
