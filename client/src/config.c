/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "config.h"
#include "defaults.h"

config_t config;		/* structure with config values */

int config_parse (char *var, char *val)
{
	if (!strcasecmp (var, CONFIG_VAR_NAME)) {
		config.name = strdup (val);
		return 0;
	}

	if (!strcasecmp (var, CONFIG_VAR_PWD)) {
		config.pwd = strdup (val);
		return 0;
	}

	if (!strcasecmp (var, CONFIG_VAR_SERVER)) {
		config.server = strdup (val);
		return 0;
	}

	if (!strcasecmp (var, CONFIG_VAR_PORT)) {
		config.port = (unsigned short) atoi (val);
		return 0;
	}

	return -1;
}

/* load config file */
int config_load (char *filename)
{
	unsigned l, line = 0;
	char *buffer, *c;
	char var[256];
	char val[256];

	/* open specified file */
	FILE *fp = fopen (filename, "r");
	
	if (!fp) {
		printf ("> WARNING -> config file '%s' not found\n", DEFAULT_CONFIG_PATH);
		return -1;
	}
		
	buffer = (char *) calloc (600, sizeof (char));

	if (!buffer)
		return -1;
	
	while (!feof (fp)) {
		/* read line */
		char *r = fgets (buffer, 600, fp);
		
		if (!r)
			continue;
		
		line ++;
		
		l = strlen (buffer);
		
		if (l < 8)
			continue;

		if (buffer[l - 1] == '\n')
			buffer[l -- - 1] = '\0';
		
		/* move to first valid letter */
		for (c = buffer; *c && *c == ' '; c ++);

		/* allow comments */
		if (*c == '#')
			continue;
		
		/* parse line */
		sscanf (c, "%s %s", var, val);
		
		config_parse (var, val);

		/* clear line */
		memset (buffer, 0, l);
	}
      
	free (buffer);
	fclose (fp);

	return 0;
}

int config_init ()
{
	memset (&config, 0, sizeof (config_t));
  
	int ret = config_load (DEFAULT_CONFIG_PATH);

	if (!ret && (!config.name || !config.pwd || !config.server)) {
		printf ("ERROR -> config file is not valid !\nPlease rewrite " DEFAULT_CONFIG_PATH " in this way:\n\n"
			"\tname YOURNAME\n"
			"\tpassword YOURPASSWORD\n"
			"\tserver TUNNELSERVER\n"
			"\tport 6060\n");

		return -1;
	}
	
	return ret;
}
