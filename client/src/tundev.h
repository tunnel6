/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _TUNDEV_H
#define _TUNDEV_H

extern int tundev_arch_poll ();
extern int tundev_arch_send (char *packet, unsigned len);
extern int tundev_arch_recv ();
extern int tundev_arch_setup ();
extern int tundev_arch_init ();
extern int tundev_poll ();
extern int tundev_send (char *packet, unsigned len);
extern int tundev_setup ();
extern int tundev_init ();

#endif
