/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __WIN32__
# include <sys/socket.h>
# include <netinet/in.h>
# include <arpa/inet.h>
# include <string.h>
#else
# include <windows.h>
# include <ws2tcpip.h>
#endif
#include <stdio.h>
#include "ipv6.h"
#include "packet.h"
#include "defaults.h"

static char ipv6[64];			/* temporary buffer used by ipv6_print () */
#ifdef __WIN32__
extern const char *inet_ntop (int af, const void *src, char *dst, socklen_t cnt);
#else
static int fd;				/* FD for IPv6 packet sender */
#endif

/* compare two IPv6 address */
unsigned ipv6_cmp (ipv6_addr_t ip, ipv6_addr_t ip2)
{
	unsigned i;
	for (i = 0; i < 8; i ++)
		if (ip[i] != ip2[i])
			return 0;

	return 1;
}

/* compare two IPv6 prefix address */
unsigned ipv6_cmp_prefix (ipv6_prefix_t pr, ipv6_prefix_t ip)
{
	/* check valid prefix length */
	if (!pr[15])
		return 0;
  
	unsigned i;
	for (i = 0; i < (pr[15]/8); i ++)
		if (pr[i] != ip[i])
			return 0;

	return 1;
}

/* print IPv6 address into buffer */
char *ipv6_print (ipv6_addr_t ip)
{
	if (!inet_ntop (AF_INET6, ip, ipv6, 64))
		return 0;
	
	return ipv6;
}

#ifndef __WIN32__
int ipv6_send (char *data, int len)
{
	struct sockaddr_in6 sockaddr;
	struct proto_ipv6_t *ip = (struct proto_ipv6_t *) data;

	/* prepare data for RAW socket */
	sockaddr.sin6_family = AF_INET6;
	sockaddr.sin6_port = 0;
	memcpy (&sockaddr.sin6_addr.s6_addr, ip->dest, sizeof (ipv6_addr_t));

	/* send the packet */
	if (sendto (fd, data, len, 0, (struct sockaddr *) &sockaddr, sizeof (struct sockaddr_in6)) != len) {
		perror ("sendto()");
		return -1;
	}
	
	return 0;
}
#endif
int ipv6_init ()
{
#ifndef __WIN32__
	/* initialize raw socket */
	if ((fd = socket (AF_INET6, SOCK_RAW, IPPROTO_RAW)) == -1) {
		perror ("socket()");
		return -1;
	}
#endif
	return 0;
}
