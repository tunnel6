/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include "db.h"
#include "ndp.h"
#include "ipv6.h"
#include "sniff.h"
#include "packet.h"
#include "checksum.h"

extern struct netdev_t netdev;

/* send neighbour advertisement - we lie about existing address  :-B */
int ndp_reply (struct proto_eth_t *eth, char *data)
{
	struct proto_ipv6_t *ip = (struct proto_ipv6_t *) data;
	struct proto_ndp_t *ndp = (struct proto_ndp_t *) ((char *) data + sizeof (struct proto_ipv6_t));

	/* check for neighbour solicitation packet */
	if (ndp->type != NDP_TYPE_NBSOL)
		return -1;
	
	/* check target with database */
	client_t *c = db_check (ndp->target);
	
	if (!c)
		return 0;

	if (!c->info)
		return 0;
	
	/* build a new packet */
	char packet[sizeof (struct proto_eth_t) + sizeof (struct proto_ipv6_t) + sizeof (struct proto_ndp_t) + 1];
	
	/* ethernet layer */
	struct proto_eth_t *packet_eth = (struct proto_eth_t *) &packet;
	
	memcpy (&packet_eth->src, netdev.hwaddr, sizeof (mac_addr_t));
	memcpy (&packet_eth->dest, eth->src, sizeof (mac_addr_t));
	packet_eth->type = PACKET_TYPE_IPV6;
	
	/* IPv6 layer */
	struct proto_ipv6_t *packet_ip = (struct proto_ipv6_t *) ((char *) &packet + sizeof (struct proto_eth_t));
	
	packet_ip->ver = 0x60;
	packet_ip->tclass = 0x0;
	packet_ip->flabel = 0x0;
	packet_ip->pl_len = htons (32);
	packet_ip->nhead = IPV6_TYPE_ICMPv6;
	packet_ip->hop = 0xff;
	memcpy (packet_ip->src, (void *) ndp->target, sizeof (ipv6_addr_t));
	memcpy (packet_ip->dest, (void *) ip->src, sizeof (ipv6_addr_t));
	
	/* ICMPv6 - NDP layer */
	struct proto_ndp_t *packet_ndp = (struct proto_ndp_t *) ((char *) &packet + sizeof (struct proto_eth_t) + sizeof (struct proto_ipv6_t));

	packet_ndp->type = NDP_TYPE_NBADV;
	packet_ndp->code = 0;
	packet_ndp->flags = htonl (0x60000000);
	memcpy (packet_ndp->target, (void *) ndp->target, sizeof (ipv6_addr_t));

	packet_ndp->ll_type = 2;
	packet_ndp->ll_length = 1;
	memcpy (&packet_ndp->ll_mac, netdev.hwaddr, sizeof (mac_addr_t));
	packet_ndp->checksum = 0x0;
	
	/* calculate checksum */
	packet_ndp->checksum = checksum16_ipv6 (packet_ip->src, packet_ip->dest, (char *) packet_ndp, sizeof (struct proto_ndp_t), IPV6_TYPE_ICMPv6);
	
	/* send RAW packet */
	return sniff_send (packet, sizeof (struct proto_eth_t) + sizeof (struct proto_ipv6_t) + sizeof (struct proto_ndp_t));
}

int ndp_init ()
{

	return 0;
}
