<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<meta name="Description" content="Tunnel, IPv6, connectivity" />
<meta name="Keywords" content="tunnel, ipv6, connectivity" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Distribution" content="Global" />
<meta name="Author" content="Tomáš Jędrzejek" />
<meta name="Robots" content="index,follow" />

<link rel="stylesheet" href="images/CoolWater.css" type="text/css" />

<title>Tunnel6 provider</title>
	
</head>

<?php
$provider = "Infern0 #CZ";
$t6db = "/etc/t6db.conf";
?>

<body>
<!-- wrap starts here -->
<div id="wrap">
		
	<!--header -->
	<div id="header">			
				
		<h1 id="logo-text"><a href="index.php">Tunnel6 server</a></h1>		
		<p id="slogan">by <?php echo "$provider"; ?></p>
			
	</div>
		
	<!-- navigation -->	
	<div  id="menu">
		<ul>
			<li id="current"><a href="index.php">Home</a></li>
			<li><a href="reg.php">Register</a></li>
			<li><a href="http://tunnel6.zexos.org/">Tunnel6</a></li>	
		</ul>
	</div>					
			
	<!-- content-wrap starts here -->
	<div id="content-wrap">
		
		<div id="main">				
				
			<a name="Info"></a>
			<h2><a href="index.php">Tunnel6 provider</a></h2>
					
			<p class="post-by">posted by: <a href="index.php">ZeXx86</a></p>
				
            <p><strong><?php echo "$provider"; ?></strong> provides service to all who are not able to connect via native IPv6 to the internet.
	    We brings you alternative solution that offer <a href="http://tunnel6.zexos.org">Tunnel6</a> technology.
            </p>
	    
            <p>
            Tunnel6 uses UDP protocol atop IPv4 layer what makes tunnel able to carry IPv6 packets.
	    It should go through all NATs and standard firewalls, so <i>client don't need any IPv4 public address</i>. 
            </p>

            <p>
	    You can easily <a href="reg.php">register</a> your account and enjoy IPv6 everywhere you want !
            </p>

            <p>
	  
	    <?php 
		    $c = 0;
		    $data = File ($t6db);
		    for ($i = 0; $i < Count ($data); $i++) {
			  if (strlen ($data[$i]) > 3)
				if (!strstr ($data[$i], "#"))
					$c ++;
		    }

		    if ($c > 1)
			  echo "$provider is providing IPv6 tunnel for $c clients at this moment, you can be next.";
	    ?> 
	    
            </p>

		</div>
		
			
		<div id="sidebar">
			
			<h2>Sidebar Menu</h2>
			<ul class="sidemenu">				
				<li><a href="http://tunnel6.zexos.org/#info">What is Tunnel6</a></li>
				<li><a href="http://tunnel6.zexos.org/#works">How it works</a></li>
				<li><a href="http://tunnel6.zexos.org/#platforms">Supported platforms</a></li>
			</ul>	
				
			<h2>Links</h2>
			<ul class="sidemenu">
				<li><a href="http://www.ipv6portal.cz">IPv6 Portal</a></li>
				<li><a href="http://www.zexos.org">ZeX/OS</a></li>
				<li><a href="http://www.rd-hosting.eu">RD-Hosting</a></li>
			</ul>
			
			<h2>Support IPv6</h2>
			<p><a href="http://en.wikipedia.org/wiki/IPv6">Internet Protocol version 6</a> (IPv6) is an Internet Protocol version which will succeed IPv4, the first implementation which is still in dominant use currently</p>

			<p><form action="https://www.paypal.com/cgi-bin/webscr" method="post">
			<input type="hidden" name="cmd" value="_s-xclick">
			<input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----MIIHLwYJKoZIhvcNAQcEoIIHIDCCBxwCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYAvzl20Ki1vmOpNW5xNEh1IRYScf7agugOzNc8Ii/JnkcYgBJBRrJE9RVRpbZpjgyVwFcOWYYTWOkJ2c6FnVUYYU6WX3EyBBDoEes/UMtfi68l2aq8O0vbGk1C8oxfHl/zYiBDbWE2rffx/F/+UXZppGjJ2h76q+QOx5rMrgzXo+TELMAkGBSsOAwIaBQAwgawGCSqGSIb3DQEHATAUBggqhkiG9w0DBwQI88JhWHMcGW6AgYjMxV3MlAStqQwhINIj93+gw7cKam2VHM4yw5gnurr1z3yeIpoQy5OXMfoc277sXo+95QA/1q3VxXdeCK0Raz+C4cpysqqkZfJgAKFNEn0vZTzjgs+f/UZjRQLq1r6m+lvgQvpRgUivTlIx505Cwh52QVTH6e0acIW+AVssrSl0P+xBz9g9L1qRoIIDhzCCA4MwggLsoAMCAQICAQAwDQYJKoZIhvcNAQEFBQAwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMB4XDTA0MDIxMzEwMTMxNVoXDTM1MDIxMzEwMTMxNVowgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDBR07d/ETMS1ycjtkpkvjXZe9k+6CieLuLsPumsJ7QC1odNz3sJiCbs2wC0nLE0uLGaEtXynIgRqIddYCHx88pb5HTXv4SZeuv0Rqq4+axW9PLAAATU8w04qqjaSXgbGLP3NmohqM6bV9kZZwZLR/klDaQGo1u9uDb9lr4Yn+rBQIDAQABo4HuMIHrMB0GA1UdDgQWBBSWn3y7xm8XvVk/UtcKG+wQ1mSUazCBuwYDVR0jBIGzMIGwgBSWn3y7xm8XvVk/UtcKG+wQ1mSUa6GBlKSBkTCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb22CAQAwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQUFAAOBgQCBXzpWmoBa5e9fo6ujionW1hUhPkOBakTr3YCDjbYfvJEiv/2P+IobhOGJr85+XHhN0v4gUkEDI8r2/rNk1m0GA8HKddvTjyGw/XqXa+LSTlDYkqI8OwR8GEYj4efEtcRpRYBxV8KxAW93YDWzFGvruKnnLbDAF6VR5w/cCMn5hzGCAZowggGWAgEBMIGUMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbQIBADAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMDkwMTE2MjEzNTQwWjAjBgkqhkiG9w0BCQQxFgQU9HhvbrzAoT1gGXgKR0Joe4D2VxQwDQYJKoZIhvcNAQEBBQAEgYALZEwnlDHV9TDjizfeqF6UtJAKMFbfr719hjLBFqMHtmnQPJzz6URllIgI8N+JMLO9Ijw6cinAL9tQEGamhDi1WJwojahLo3s+0meRCs5FEgQPsXQp/j73bDhz4He97uv4HLNTRhgus3ZIGnamkLW2KeajUJR2bNz22UtLs10D1A==-----END PKCS7-----
			">
			<input type="image" src="https://www.paypal.com/en_GB/i/btn/btn_donate_LG.gif" border="0" name="submit" alt="">
			</form></p>
		</div>
				
	<!-- content-wrap ends here -->	
	</div>
					
	<!--footer starts here-->
	<div id="footer">	
		<p>
		&copy; 2010 <strong>ZeXx86</strong> |
		<a href="http://www.bluewebtemplates.com/" title="Website Templates">website templates</a> by <a href="http://www.styleshout.com/">styleshout</a> |
		Valid <a href="http://validator.w3.org/check?uri=referer">XHTML</a> | 
		<a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a>   		
   	</p>
				
	</div>	

<!-- wrap ends here -->
</div>

</body>
</html>
