/*
 *  tunnel6
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "db.h"
#include "ipv6.h"
#include "config.h"
#include "proto.h"
#include "base64.h"
#include "defaults.h"

client_t client_list;
struct timeval tdb;
extern struct timeval tnow;
extern config_t config;		/* structure with config values */

static db_backend_t *db_backend;
extern db_backend_t db_file;
extern db_backend_t db_mysql;

int db_add (ipv6_addr_t ip, ipv6_prefix_t prefix, char *name, char *pwd)
{
	/* alloc and init context */
	client_t *client = (client_t *) malloc (sizeof (client_t));

	if (!client)
		return -1;

	memcpy (client->ip, (void *) ip, sizeof (ipv6_addr_t));
	memcpy (client->prefix, (void *) prefix, sizeof (ipv6_prefix_t));
	client->name = strdup (name);
	client->pwd = strdup (pwd);
	client->info = 0;
		
	/* add into list */
	client->next = &client_list;
	client->prev = client_list.prev;
	client->prev->next = client;
	client->next->prev = client;
	
	printf ("DB -> added '%s'\n", name);
	
	return 0;
}

int db_update (client_t *c, ipv6_addr_t ip, ipv6_prefix_t prefix, char *pwd)
{
	/* change ipv6 address when it is different */
	if (!ipv6_cmp (c->ip, ip)) {
		memcpy (c->ip, (void *) ip, sizeof (ipv6_addr_t));
		
		printf ("DB -> IPv6 updated for '%s'\n", c->name);
	}
	
	/* change prefix address when it is different */
	if (!ipv6_cmp ((unsigned short *) c->prefix, (unsigned short *) prefix)) {
		memcpy (c->prefix, (void *) prefix, sizeof (ipv6_prefix_t));
		
		printf ("DB -> prefix updated for '%s'\n", c->name);
	}
	
	/* change password when it is different */
	if (strcmp (c->pwd, pwd)) {
		/* free old memory block */
		free (c->pwd);
		
		c->pwd = strdup (pwd);
		
		printf ("DB -> password updated for '%s'\n", c->name);
	}
	
	return 0;
}

int db_disconnect_all ()
{
	client_t *client;
	for (client = client_list.next; client != &client_list; client = client->next) {
		if (!client->info)
			continue;

		proto_cmd_disconnect2 (client);
	}
  
	return 0;
}

client_t *db_check (ipv6_addr_t ip)
{
	client_t *client;
	for (client = client_list.next; client != &client_list; client = client->next) {
		if (ipv6_cmp (client->ip, ip))		/* compare client ipv6 */
			return client;
		else 	/* compare client prefix */
		if (ipv6_cmp_prefix ((unsigned char *) client->prefix, (unsigned char *) ip))
			return client;
	}
	
	return 0;
}

client_t *db_find (struct sockaddr_in *info)
{
	client_t *client;
	for (client = client_list.next; client != &client_list; client = client->next) {
		if (!client->info)
			continue;

		if (!memcmp (client->info, info, sizeof (struct sockaddr_in)))
			return client;
	}
	
	return 0;
}

client_t *db_find_byname (char *name)
{
	client_t *client;
	for (client = client_list.next; client != &client_list; client = client->next) {
		if (!strcmp (client->name, name))
			return client;
	}
	
	return 0;
}

client_t *db_verify (char *name, char *pwd)
{
	client_t *client;
	for (client = client_list.next; client != &client_list; client = client->next) {
		if (!strcmp (client->name, name))
			if (!strcmp (client->pwd, pwd))
				return client;
	}
	
	return 0;
}

/* clean clients info */
int db_flush (client_t *c)
{
	if (!c->info)
		return -1;
  
	void *p = (void *) c->info;
	
	c->info = 0;
	
	free (p);

	return 0;
}

/* load database of clients from file */
int db_load ()
{
	return db_backend->load ();
}

/* save database of clients */
int db_save ()
{
	return db_backend->save ();
}

/* reload database on demand */
int db_reload ()
{
	if (timercmp (&tnow, &tdb, <))
		return 0;
	
	tdb = tnow;
	tdb.tv_sec += DEFAULT_DATABASE_UPDATE;

	return db_backend->reload ();
}

int db_init ()
{
	client_list.next = &client_list;
	client_list.prev = &client_list;

	db_backend = &db_file;
#ifdef WITH_MYSQL
	if (config.backend)
	if (!strcmp (config.backend, "mysql"))
		db_backend = &db_mysql;
#endif
	/* null modify time for first time */
	if (db_backend->init () == -1)
		return -1;
	
	tdb = tnow;

	return 0;
}
